function f=plot_zhang(k0,t,tplot,X,u,B0,T0)


		L0=2*pi./k0;
		f=figure('position',[20 20 500 500]);
		s=pcolor(t(tplot)./T0,fliplr(X./L0),flipud(abs(u(tplot,:)'))./B0);
		set(s, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
	% 	set(s,'ytick',[0 20 ] ,'xtick',[ 0 min(H.*K)  max(H.*K)*1.0000001 ],'xticklabel',{'hk:' min(H.*K)  max(H.*K)})
		set(gca,'ytick',[0 20 40 60],'yticklabel',{0 20 40 60},'xtick',[-16 -8 0 8 16],'xticklabel',{-16 -8 0 8 16})

		 xlabel('\tau/T_0  ','fontsize',16); 
		 ylabel('x/L_{0}  ','fontsize',16);
		 xlim([-16,16])
		 ylim([0,60])
		colorMap = jet;
% 			colorMap = brighten(colorMap,0.3);
		colormap(colorMap);   % Apply the colormap
		cb=colorbar();
		ylabel(cb, '|U|/a_0')
		caxis([0 2])


end