function [u4,err] = DV4ORK34_spacelike_AD_simple(ht,u,kx,h,lambda0,nu0,alpha0,alpha40,beta0,gamma0,dythe_hilb0,kh0)
% adaptive embedded RK4(3) method for solving the variable depth HONLS in the Interaction Picture
% INPUT
% hz: current step size
% u: complex field
% kx: frequency bins
%
% BEWARE the IP integrating factor now involves an integral of lambda in
% the expontential and the multiplication by a factor on account of
% shoaling.
%
% OUTPUT: function value at z + hz, correcting hz0 accordingly
%
% Andrea Armaroli 26.02.19 
% Coefficients come from Sedletsky work. No nonlocality is present in the
% finite depth case
%Added hilbert term
% 02.04.19 Included 4OD to suppress the resonant-radiation
%%
persistent k5
n = size(kx,1);
mask = ones(n,1);
% mask(round(6*n/14:8*n/12-1)) = 0;


 % integrating factor x -> x + hx/2
phaselin = exp(-1i.*(kx.^2.*(lambda0)'./2 + kx.^3.*(alpha0)' + kx.^4.*(alpha40 )').*ht/2);

    
uIP = fft((phaselin.*(ifft(u).*mask)));
if isempty(k5)
    Ncurr = nonlinear(u,kx, nu0, beta0, gamma0,dythe_hilb0,h);
    k1 = fft(phaselin.*(ifft(Ncurr)));
else
    k1 = fft(phaselin.*(ifft(k5)));
end

	
k2 = nonlinear(uIP + ht/2.*k1 ,kx, nu0, beta0, gamma0,dythe_hilb0,h);
k3 = nonlinear(uIP + ht/2.*k2 ,kx,  nu0, beta0, gamma0,dythe_hilb0,h);
u2 = fft(phaselin.*(ifft(uIP + ht.*k3)));
k4 = nonlinear(u2, kx, nu0, beta0, gamma0,dythe_hilb0,h);
beta = fft(phaselin.*ifft(uIP + ht/6.*(k1 + 2*k2 + 2*k3)));
u4 = beta + ht/6*k4;
k5 = nonlinear(u4, kx, nu0, beta0, gamma0,dythe_hilb0,h);
u3 = beta + ht/30.*(2*k4+3*k5);
err = norm(u3-u4);


end 

function [uNL] = nonlinear(u, kx, nu, beta, gamma,dythe_hilb,h)
% NLflag = 1;
uNL3O = nu*abs(u).^2.*u;
uNL4O = beta.*abs(u).^2.*fft(-1i.*kx.*ifft(u)) + gamma.*u.^2.*fft(-1i.*kx.*ifft(conj(u)))+dythe_hilb.*1i.*u.*fft(abs(kx)./(tanh(h.*kx)).*ifft(abs(u).^2)) ;
uNL = -(1i.*uNL3O - uNL4O);
end
