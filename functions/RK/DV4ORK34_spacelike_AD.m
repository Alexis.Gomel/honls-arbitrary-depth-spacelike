function [u4,err] = DV4ORK34_spacelike_AD(ht,u,kx,h,lambda0,lambda1,lambda2,nu0,nu1,nu2,alpha0,alpha1,alpha2,alpha40,alpha41,alpha42,beta0,beta1,beta2,gamma0,gamma1,gamma2,muintexp0,muintexp1,muintexp2,dythe_hilb0,dythe_hilb1,dythe_hilb2,shoalflag,dis,kh0,kh1,kh2)
adaptive embedded RK4(3) method for solving the variable depth HONLS in the Interaction Picture
INPUT
hz: current step size
u: complex field
kx: frequency bins
lambda0, 1, 2 are the values of lambda at x, x+hx/2, x + hx
mn0, 1, 2 are the values of nu at x, x+hx/2, x + hx
alpha0, 1, 2 are the values of alpha (TOD) at x, x+hx/2, x + hx
alpha40, 1, 2 are the values of alpha (FOD) at x, x+hx/2, x + hx 
beta0, 1, 2 are the values of beta (|A|^2 A_t) at x, x+hx/2, x + hx
gamma0, 1, 2 are the values of beta (A^2 A*_t) at x, x+hx/2, x + hx

We limit ourselves to the Djordjevic Redekopp shoaling
muintexp0, 1, 2 are the values of exponential of the integral of shoaling at x, x+hx/2, x + hx
shoalflag allows to switch shoaling off (it could be useful to compare to fibres)

BEWARE the IP integrating factor now involves an integral of lambda in
the expontential and the multiplication by a factor on account of
shoaling.

OUTPUT: function value at z + hz, correcting hz0 accordingly

Andrea Armaroli 26.02.19 
Coefficients come from Sedletsky work. No nonlocality is present in the
finite depth case
Added hilbert term
02.04.19 Included 4OD to suppress the resonant-radiation
%
persistent k5
n = size(kx,1);
mask = ones(n,1);
mask(round(6*n/14:8*n/12-1)) = 0;

if shoalflag == 1
    integrating factor x -> x + hx/2
    phaselin1 = muintexp0./muintexp1.*exp(-1i.*(kx.^2.*(lambda0+lambda1)./2 + kx.^3.*(alpha0 + alpha1)/2 + kx.^4.*(alpha40 + alpha41)/2 ).*ht/2);
    
    integrating factor x+ hx/2 -> x + hx
    phaselin2 = muintexp1./muintexp2.*exp(-1i.*(kx.^2.*(lambda2+lambda1)./2 + kx.^3.*(alpha1+alpha2)/2 + kx.^4.*(alpha41 + alpha42)/2 ).*ht/2);

else 
     integrating factor x -> x + hx/2
    phaselin1 = exp(-1i.*(kx.^2.*(lambda0+lambda1)'./2 + kx.^3.*(alpha0 + alpha1)'/2 + kx.^4.*(alpha40 + alpha41)'/2 ).*ht/2);
    
    integrating factor x+ hx/2 -> x + hx
    phaselin2 = exp(-1i.*(kx.^2.*(lambda2+lambda1)'./2 + kx.^3.*(alpha1+alpha2)'/2 + kx.^4.*(alpha41 + alpha42)'/2 ).*ht/2);

end
    
uIP = fft((phaselin1.*(ifft(u).*mask)));
if isempty(k5)
    Ncurr = nonlinear(u,kx, nu0, beta0, gamma0,dis,dythe_hilb0,h,kh0);
    k1 = fft(phaselin1.*(ifft(Ncurr)));
else
    k1 = fft(phaselin1.*(ifft(k5)));
end

	
k2 = nonlinear(uIP + ht/2.*k1 ,kx, nu1, beta1, gamma1,dis,dythe_hilb1,h,kh1);
k3 = nonlinear(uIP + ht/2.*k2 ,kx,  nu1, beta1, gamma1,dis,dythe_hilb1,h,kh1);
u2 = fft(phaselin2.*(ifft(uIP + ht.*k3)));
k4 = nonlinear(u2, kx, nu2, beta2, gamma2,dis,dythe_hilb2,h,kh2);
beta = fft(phaselin2.*ifft(uIP + ht/6.*(k1 + 2*k2 + 2*k3)));
u4 = beta + ht/6*k4;
k5 = nonlinear(u4, kx, nu2, beta2, gamma2,dis,dythe_hilb2,h,kh2);
u3 = beta + ht/30.*(2*k4+3*k5);
err = norm(u3-u4);


end 

function [uNL] = nonlinear(u, kx, nu, beta, gamma,d,dythe_hilb,h,kh)
NLflag = 1;
uNL3O = nu*abs(u).^2.*u;
uNL4O = beta.*abs(u).^2.*fft(-1i.*kx.*ifft(u)) + gamma.*u.^2.*fft(-1i.*kx.*ifft(conj(u)))+dythe_hilb.*1i.*u.*fft(abs(kx)./(tanh(h.*kx)).*ifft(abs(u).^2)) ;
uNL = -(1i.*uNL3O - uNL4O)- d.*u;
end
