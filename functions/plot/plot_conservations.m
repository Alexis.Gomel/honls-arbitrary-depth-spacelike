function f = plot_conservations(X,N,P)

f=figure('name','Conservations');
subplot(311);
plot(X,N,'linewidth',1.5);
xlabel('X','fontsize',16);
set(gca,'FontSize',14);
ylabel('N/N(0)','fontsize',16);
subplot(312);
plot(X,P,'linewidth',1.5);
set(gca,'FontSize',14);
xlabel('X','fontsize',16);
ylabel('P/P(0)','fontsize',16);
subplot(313);
plot(X(2:end),diff(real(P./N)),'linewidth',1.5);
set(gca,'FontSize',14);
xlabel('X','fontsize',16);
ylabel('d(N/N0)/dx','fontsize',16);
% suptitle('Conservation of momenta');

end