function [NLS_dispersion, NLS_nonlinear, HO_dispersion, HO_current, HO_asymetry]=NLS_coef_dialg()

titles={'NLS Dispersion','NLS Nonlinear','HO Dispersion','HO current','HO Asymmetry'}
prompt=titles
dlg_title = 'Equation coeffiecients ON/OFF';
options.Interpreter = 'tex';
num_lines = 1;
if ~exist('.\button_dialog_temp_1.txt', 'dir') %check if dir already exists
def_coef_dialg=importfile_eq_coef('.\default_coef_equation_temp.txt');
def_coef_dialg=cellstr(def_coef_dialg);
NLS_coef=inputdlg(prompt,dlg_title,1,def_coef_dialg)
else
def_coef_dialg = {'1','1','1','1','1'};
NLS_coef=inputdlg(titles,dlg_title,1,def_coef_dialg)
end
NLS_dispersion = str2double(NLS_coef(1));
NLS_nonlinear = str2double(NLS_coef(2));
HO_dispersion = str2double(NLS_coef(3));
HO_current = str2double(NLS_coef(4));
HO_asymetry = str2double(NLS_coef(5));
print_eq_coef(NLS_dispersion,NLS_nonlinear,HO_dispersion,HO_current,HO_asymetry)
