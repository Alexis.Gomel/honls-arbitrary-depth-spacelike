function [NLS_coef]=onoff_dialog()
	
	titles={'NLS Dispersion','NLS Nonlinear','HO Dispersion','HO current','HO Asymmetry','Dysthe Hilbert'};
	dlg_title = 'Equation coeffiecients ON/OFF';
	options.Interpreter = 'tex';
	if ~exist('button_dialog_temp_1.txt', 'dir') %check if dir already exists
		def_coef_dialg=importfile_eq_coef('default_coef_equation_temp.txt');
		def_coef_dialg=cellstr(def_coef_dialg);
% 		fprintf('exist')
		NLS_coef=inputdlg(titles,dlg_title,1,def_coef_dialg);
	else
		def_coef_dialg = {'1','1','1','1','1'};
		NLS_coef=inputdlg(titles,dlg_title,1,def_coef_dialg);
	end

end