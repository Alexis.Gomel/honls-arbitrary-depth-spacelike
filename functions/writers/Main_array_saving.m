% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% file exporter for data plotting.
% todo:
% -add windowing ?
% -extension plots 
% -envelope with filter                 ----done
% -high order steeptness Cg
% -3d plots                             ----60%
% -3d plots fourier                     ----30%
% -add first signal
% -2d plot                              ----40%
% -make Cg depth dependent              ----done
% -compare envelopes with simulations.
% -steepness plot                       ----60%
% -changing k                           ----done
% -check hunt dissipation
% -maybe separate data preparation from plotting.
% -add plot without dissipation         ----50%
% -add sanity check: hilbert with and without filter.       ----done
% -add sanity check: nonlinear and dispersive lenghts, check relationship
% -add sanity check: real steepness vs filtered steepness.
% -add sanity check: simul amplitude, real amplitude
% -add sanity check: plot rho initial cond.  with and without filtering
% -add visibility
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
clear all;
close all;
clc;
windowing=1; %set to 1 to use windows for the signal
highpass_freq = 0.7; %low freq limit bandpass
%% read last file used. 
currentFolder = pwd; 
try %in case this file doesn't exist
    fileID = fopen('Last_file_temp.txt','r');
    old_filename=string(textscan(fileID,'%s'));%read lastname used
    fclose(fileID);
catch exception
    fileID = fopen('Last_file_temp2.txt','r');
    old_filename=string(textscan(fileID,'%s'));%read lastname used
    fclose(fileID);
end

%% prompt to choose file for treating data.
d = dir(strcat(currentFolder,'\data\'));
isub = [d(:).isdir]; %# returns logical vector
nameFolds = {d(isub).name}';
nameFolds(ismember(nameFolds,{'.','..'})) = [];
list=nameFolds;
try
    nameidx=find(contains(list,old_filename));
catch exception
    nameidx=0;
end   
[indx,tf] = listdlg('PromptString','Select a file:', 'SelectionMode','single','ListSize',[600 300],'InitialValue',nameidx,'ListString',list );
filename=string(list(indx));
print_filename(filename);
folder = strcat(currentFolder,'\data\');
fullfolder = strcat(folder,filename);
if ~exist(strcat(fullfolder,'\Post-analysis\'), 'dir') %check if dir already exists
       mkdir(strcat(fullfolder,'\Post-analysis\'))%makes the folder for preeliminary results.
end
% mkdir(strcat(fullfolder,'\Pre-analysis\')) 
%% make post analysis folder-
Post_analysis_folder=strcat(fullfolder,'\Post-analysis\',filename);
datafile = strcat(fullfolder,'\',filename,'_data.txt');
[measured_data,file_sr] = Import_tank_file(datafile);% data, Hz  files sampling rate: aqusition rate waveguages this is in the header
newposfile=0;
nobathfile=0;
try %check if there is a file ending in _data_2nd. This file is for new position of the wavegauges
    datafile_newpos = strcat(fullfolder,'\',filename,'_data_2nd.txt');
    [measured_data_newpos,file_sr_newpos] = Import_tank_file(datafile_newpos);% data, Hz  files sampling rate: aqusition rate waveguages this is in the header
    newposfile=1;
end
try %check if there is a file ending in _data10. This file is for the experiment without bathymetry
    datafile_newpos = strcat(fullfolder,'\',filename,'_data10.txt');
    [measured_data_nobath,file_sr_nobath] = Import_tank_file(datafile_newpos);% data, Hz  files sampling rate: aqusition rate waveguages this is in the header
    nobathfile=1;
end
posfile = strcat(fullfolder,'\',filename,'_pos.txt'); %read awvegauge position files. 
aux_data = Import_tank_file_pos(posfile);
if newposfile==1 %read position of new gauges for 2nd run.
    try
        new_posfile = strcat(fullfolder,'\',filename,'_pos2.txt');
        aux_data_2 = Import_tank_file_pos(new_posfile);
    end
end
try %read bathymetry information.
    bathfile = strcat(fullfolder,'\',filename,'_bathm.txt');
    bath_table = Import_bathm_from_input(bathfile);
    lenx=table2array(bath_table(6:end,{'X'}));
    H=table2array(bath_table(6:end,{'H'}));
end

%% define arrays
%% arrays for first experiment
data = table2array(measured_data(:,{'Gauge','Gauge1','Gauge2','Gauge3','Gauge4','Gauge5','Gauge6','Gauge7'}));
wave_mkr = table2array(measured_data(:,{'Position'}));
pos_wg=table2array(aux_data(:,{'gauges_position'}));
t = linspace(0,length(data(:,1))/32,length(data(:,1))); %time array
dt=t(2);
data_size = length(t);
omegaxis = 2*pi.*linspace(-1/dt/2,1/dt/2,data_size+1); omegaxis = omegaxis(1:end-1);

if newposfile==1 %arrays for new gauge positions.
    data2 = table2array(measured_data_newpos(:,{'Gauge','Gauge1','Gauge2','Gauge3','Gauge4','Gauge5','Gauge6','Gauge7'}));
    pos_wg2=table2array(aux_data_2(:,{'gauges_position'}));  
    t2 = linspace(0,length(data2(:,1))/32,length(data2(:,1))); %time array
    dt2=t(2);
    data_size2 = length(t);
    omegaxis_2 = 2*pi.*linspace(-1/dt/2,1/dt/2,data_size+1); omegaxis = omegaxis(1:end-1);
    fprintf('check if new gauge position have the same lenght as the old positions \n ')
    fprintf('old lenght = %.1i , new lenght = %.1i \n',data_size,data_size2)
end
if nobathfile==1 
    datanobath = table2array(measured_data_nobath(:,{'Gauge','Gauge1','Gauge2','Gauge3','Gauge4','Gauge5','Gauge6','Gauge7'}));
end

%% Calculate parameters
% Parameters from filename
[run_par, run_par_value] = strread(filename, '%s %s', 'delimiter', '_');
% cut off the date values, change delimiter, convert to double 
run_par_value = run_par_value(2:end) ;
run_par_value = strrep(run_par_value, 'p', '.');
run_par_value = str2double(run_par_value);
%%
g=9.81;
T0=str2double(strrep(run_par(end), 'p', '.'));  %everything is related to the period. so this is the important value to measure. .
%%
f0=1./T0;
B0=2;%fix------------------------------------------------------------ 
amp=0.1;%sadoihas?foea?efea?fef?efe?fef?enh--------------------------
x0 = pos_wg(1); % [m] position of the first wave gauge
try
    h=abs(H(1));
catch exception
    h=2; %this will be in one file. make it so. This is the initial depth
end
n_wavegauges=size(data,2);

%% Calculate wavenumber and group velocity at start:
% carrier frequency omega
w0=2*pi/T0;  
% k0, from w^2=g*k0*tanh(k0*h)
syms k0_vpa   
k0 = -vpasolve(k0_vpa*tanh(k0_vpa*h) == (w0^2)/g, k0_vpa);
k0 = double(k0); % convert sym to number
% Group velocity
% or c_g = w0/k0; 
sigma=tanh(h.*k0);
c_g= abs(g*(sigma + k0*h*(1.-sigma^2))/(2*w0));
%Cg depth corrected

lowpass_freq = f0*1.5; %high freq limit bandpass

cg_gauge=zeros(n_wavegauges,1);
h_gauge=zeros(n_wavegauges,1);
k_gauge=zeros(n_wavegauges,1);

cg_gauge2=zeros(n_wavegauges,1);
h_gauge2=zeros(n_wavegauges,1);
k_gauge2=zeros(n_wavegauges,1);

for j=1:n_wavegauges
[ d, ix ] = min( abs( lenx - pos_wg(j) ) );%find the critical value index
h_gauge(j)=-H(ix);
syms kg_vpa   
kg = -vpasolve(kg_vpa*tanh(kg_vpa*h_gauge(j)) == (w0^2)/g, kg_vpa);
k_gauge(j) = double(kg); % convert sym to number
sigma_gauge=tanh(h_gauge(j).*k_gauge(j));
% cg_gauge(j)= abs(w0/(2*k_gauge(j)*sigma_gauge)*(sigma_gauge + k_gauge(j)*h_gauge(j)*(1.-sigma_gauge^2)));
% cg_gauge(j)=0.00001;
cg_gauge(j)= abs(g/(2.*w0))*(sigma_gauge + k_gauge(j)*h_gauge(j)*(1.-sigma_gauge^2));

end
    
%% here I concatenate stuff
if newposfile==1 %arrays for new gauge positions. 
    total_wg_pos=ones(2*n_wavegauges,1);
    total_wg_pos(1:n_wavegauges)=pos_wg;
    total_wg_pos(1+n_wavegauges:2*n_wavegauges)=pos_wg2;
    [B,I]=sort(total_wg_pos); %sort wavegauge positions to the sort gauges. 
    total_wg_pos=total_wg_pos(I);
    total_data=ones(data_size,2*n_wavegauges);
    total_data(1:data_size,1:n_wavegauges)=data;
    total_data(1:data_size,1+n_wavegauges:2n_wavegauges)=data2;
    total_data=total_data(:,I);
    
    figure()
    pcolor(total_wg_pos,t,total_data)
end
