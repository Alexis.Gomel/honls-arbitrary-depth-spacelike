function f=plot_3d_envelope(u,t,X,B0,tplot)
	f=figure('name','3d envelope');
	g0=9.81;
    meshc(t(tplot)/sqrt(g0),X,abs(u(tplot,:).')/B0);
    set(gca,'YDir','normal','FontSize',14);
    xlabel('T [s]','fontsize',16);
    ylabel('X [m]','fontsize',16);
    zlabel('|B|/B_0','fontsize',16);
    % shading interp;
    % colormap hot;
end