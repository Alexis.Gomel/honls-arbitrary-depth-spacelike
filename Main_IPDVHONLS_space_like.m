%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Variable depth NLS extended to 4th order
% See Djordjevic & Redekopp ZAMP 1978
% For the HO coefficients, see Sedletsky JETP 2003 (and same author works up to today)
%
% i B_X = -lambda B_TT + nu |B|^2B - i mu B + i (-alpha B_TTT + beta |A|^2
% A_T + gamma A^2 A^*_T) + alpha4 B_TTTT
% all coefficients depend on X in a cumbersome way, see References above 
% (Sedletsky writes the time evolution, here we employ the space evolution, coefficients change: at 4th order they involve 3rd order)
% The small parameter epsilon does NOT appear explicitely
%
% The reasonable normalization is to put g = 1, so that omega (FIXED!!!) is in unit of
% [m^-1/2] and set kh as a function of X (this IS the important parameter to discern focusing/defocusing regimes).
% Now omega^2 = k tanh kh = k sigma. We can derive h or k explicitely and
% find the dependence of cg, lambda, nu, mu. 
% omega = 1 could as well be chosen. Any different omega = pippo corresponds to a
% rescaling of time (multiply by pippo^-1) and lengths (divide by pippo^2)
%
% The solution is made by means of integrating factor or interaction
% picture. 
%
% The place of the shoaling term (mu B) is customary: the linear step is a better choice, since its integration is simpler (it is a log derivative!!!) 
% Only the 3O shoaling is included for now. The 4O is more cumbersome, but
% can be derived by simple energy arguments. 
%
% Andrea ARMAROLI, GAP University of Geneva, 26.02.2019
%UPDATE COMENTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;
clear all;
%%
addpath(genpath(fullfile('functions','writers')));
addpath(genpath(fullfile('functions','Depth_parameters')));
addpath(genpath(fullfile('functions','import')));
addpath(genpath(fullfile('functions','plot')));
addpath(genpath(fullfile('functions','colormaps')));
addpath(genpath(fullfile('functions','khfunctions')));
addpath(genpath(fullfile('functions','Dialogs')));
addpath(genpath(fullfile('functions','sidebands')));
addpath(genpath(fullfile('functions','RK')));
addpath(genpath(fullfile('functions','jonswap')));
%%
g0 = 9.81;
crit= 1.363;

%Equiation coefficients ON/Off. 1 is ON, 2 is OFF
NLS_coef=onoff_dialog();
ON_OFF_coef=onoff_coef(NLS_coef);

DimensionOptions= {'Dimensional','Adimentional'}; 
defSelection = DimensionOptions{2};
Dimensionflag = bttnChoiseDialog(DimensionOptions, 'Input Style', defSelection,...
 'Input Style'); 
fprintf( 'User selection "%s"\n',DimensionOptions{Dimensionflag});

% selecting a function for kh 
switch Dimensionflag
     case 1 
        [params]=case1_param_dialog()
		% normalized frequency omega = omega[s^-1]/g^{1/2}
        freq_dim = str2double(params(1));
        Omega = freq_dim*(2*pi)/sqrt(g0);
        T0_dim=1/freq_dim;
        fprintf('Dimentional frequency: %.3f \n , Adimentional frequency: %.3f \n ', freq_dim, Omega)
        Tmax = str2double(params(2));
        Nper = str2double(params(3));
        nx = 2.^str2double(params(4));
        % number of poinxs to save
        Nt = str2double(params(5));
        esteep0 = str2double(params(6));
        shoalingflag = str2double(params(7));
        FODflag = str2double(params(8)); 
        Exportinit =  string(params(9));
        Factor=str2double(params(10));
        dis=str2double(params(11));
        Out_time=str2double(params(12));
		%update dialog options
		print_button_dialog_parameters_case1(freq_dim,Tmax,Nper,str2double(params(4)),Nt,esteep0,shoalingflag,FODflag,Exportinit,Factor,dis,Out_time);

     case 2
        [params]=case2_param_dialog()
        % normalized frequency omega = omega[s^-1]/g^{1/2}
        Omega = str2double(params(1));
        freq_dim = Omega*sqrt(g0)/(2*pi);
        T0_dim=1/freq_dim;
        fprintf('Dimentional frequency: %.3f \n , Adimentional frequency: %.3f \n ', freq_dim, Omega)
        Tmax = str2double(params(2));
        Nper = str2double(params(3));
        nx = 2.^str2double(params(4));
        % number of points to save
        Nt = str2double(params(5));
        esteep0 = str2double(params(6));
        shoalingflag = str2double(params(7));
        FODflag = str2double(params(8));
        Exportinit =  string(params(9));
        Factor=str2double(params(10));
        dis=str2double(params(11));
        Out_time=str2double(params(12));
		%update dialog options
		print_button_dialog_parameters_case1(Omega,Tmax,Nper,str2double(params(4)),Nt,esteep0,shoalingflag,FODflag,Exportinit,Factor,dis,Out_time);
end        

%% Bathymetry
% hk is expressed as a function hk = 1.363 is the focusing/defocusing
% turning poin
% bathymetryOptions = {'Linear A->B','Erfc A->B','Linear A->B->A','Erfc A->B->A'}; 
% defSelection = bathymetryOptions{2};
% bathymetryflag = bttnChoiseDialog(bathymetryOptions, 'Bathymetry profile', defSelection,...
%  'Bathymetry profile'); 
% fprintf( 'User selection "%s"\n', bathymetryOptions{bathymetryflag});
% % selecting a function for kh 
% switch bathymetryflag
%     case 1
%         khfunsel =  @khfun4;
%     case 2
%         khfunsel =  @khfun1;
%     case 3
%         khfunsel =  @khfun3;
%     case 4
%         khfunsel =  @khfun2;
% end

khfunsel =  @khfun4;

% bathymetry params
% hk = 1.363 is the focusing/defocusing turning point
f0=freq_dim;
bathymetrypar=bathymetry_dialog_prompt()
kh0 = str2double(bathymetrypar(1));
khfin = str2double(bathymetrypar(2));
xvarstart = str2double(bathymetrypar(3)); %[m]
xvarstop = str2double(bathymetrypar(4)); %[m]
xslope = str2double(bathymetrypar(5));
print_kh_dialog_parameters(kh0,khfin,xvarstart,xvarstop,xslope)
% if bathymetryflag == 1
%     warning('Slope is not used!');
% end

%% solver params;
solvpar=solver_param_dialog();
ht0 = str2double(solvpar(1));
htmin = str2double(solvpar(2));
tol = str2double(solvpar(3));
%% definition of T lattice
Tmax=Tmax.*sqrt(g0); %transformation due to the adimentional frequency
if ht0<Tmax./Nt
    ht0 = Tmax./Nt;
end
Ht = Tmax./Nt;
T = 0:Ht:Tmax;
% Nper = 20;

%% compute the NLS parameters 
% DO NOT DISPLACE IT, it is necessary to estimate MI, soliton, DSW params
[H,K,Sigma,Cg,Cp,Lambda,Nu,Alpha,Alpha4,Beta,Gamma,Mu,Muintexp,dythe_hilb] = depthdependent_HONLSparameters_onoff(Omega,khfunsel(ones(nx+1,1),kh0,khfin,xvarstart,xvarstop,xslope),FODflag,ON_OFF_coef);
% plot_params_general(T,H,K,Mu,Cp,Cg,Muintexp,Nu,Lambda,Alpha,Alpha4,Beta,Gamma)
% ...and their initial values
[h0,k0,sigma0,cg0,cp0,lambda0,nu0,alpha0,alpha40,beta0,gamma0,mu0,muintexp0,dythe_hilb0] = depthdependent_HONLSparameters_onoff(Omega,kh0,FODflag,ON_OFF_coef);
% Reference amplitude
U0 = esteep0./k0;%dummy var
wvl=2.*pi./k0;
% Normalization scales
Lnl = sqrt(abs(2*lambda0./nu0))./U0;
Tnl = 1./abs(nu0)/U0^2;
% a0=esteep0./(k0/sqrt(2));
% L0 = (2*esteep0*k0)^-1;
L0=Lnl;

kas0 = sqrt(abs(nu0/lambda0)).*U0;
L0 = 2*pi/kas0;
% calculate the time window span
Lx = L0*Nper;
x = linspace(-Lx/2,Lx/2,nx+1);
x = x(1:end-1);

%%folders
[flm fullfolder]=save_filename(freq_dim,U0,esteep0,T0_dim);
mkdir(fullfolder) ;
filename_input=fullfile(fullfolder,strcat(flm,'_input.txt'));
% print_input_file2(filename_input,Input_function,sample_frequency,initial_depth,Factor,M);
% filename_wm=fullfile(fullfolder,strcat(flm,'_run_001.txt'));
% csvwrite(filename_wm,M) ;
filename_pos=fullfile(fullfolder,strcat(flm,'_pos.txt'));
% print_pos_file(filename_pos,Input_function,sample_frequency,initial_depth,Factor,pos_gau);
mkdir(fullfile(fullfolder,'simulation and input'))
img_folder=fullfile(fullfolder,'simulation and input');
filename_params=fullfile(fullfolder,strcat(flm,'_params.txt'));
% print_params_file(filename_params,Input_function,sample_frequency, save_params,save_params_names)
img_folder=fullfile(fullfolder,'simulation and input');
%% excitation type
inputOptions = {'Shot noise like perturbed plane wave','Harmonically perturbed plane wave','Akhmediev Breather','Soliton','Harmonically p. p-w. mine','test plane wave'}; 
defSelection = inputOptions{2};
excflag = bttnChoiseDialog(inputOptions, 'Type of initial conditions', defSelection,...
 'Type of initial conditions'); 
fprintf( 'User selection "%s"\n', inputOptions{excflag});
%excflag = questdlg('Excitation','Type of initial conditions','Shot noise like perturbed plane wave','Harmonically perturbed plane wave','Akhmediev Breather','Soliton','Harmonically perturbed plane wave');
switch excflag
    case 1
        prompt = {'Total initial energy (normalized)','Noise fraction'};
        dlg_title = 'Noisy Plane Wave';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'1','1e-2'};
        IC_case=1;
        NPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        etanoise =  str2double( NPWpar(2));
        eta0 = 1-etanoise;
        A0 = str2double( NPWpar(1));
        Kappa0 = sqrt(2*A0).*(1-1.5*sqrt(2*A0)*esteep0);
        variance = etanoise/nx*2/pi;
        waitfor(msgbox('The period length in x is chosen to match the maximum instability mode','Plane wave on noise background'));
        
    case 2
        prompt = {'Total initial energy','Sideband fraction','Sideband unbalance','Relative phase (\phi_1+\phi_{-1})/2-\phi_0','\Kappa/\Kappa_{Max}'};
        dlg_title = 'Harmonically perturbed Plane Wave';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'1','1e-2','0e-3','pi/2','1'};
        IC_case=2;
        PPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        
        A0 = str2double( PPWpar(1));
        eta0 = A0 - str2double(PPWpar(2));
        alpha = str2double(PPWpar(3));
        psi0 = (eval(char(PPWpar(4))));
        Kappa0 = str2double(PPWpar(5))*sqrt(2*A0).*(1-1.5*sqrt(2*A0)*esteep0);
		windowwidth = 10*Lnl;

        eta1 = (A0-eta0+alpha)/2;
        etam1 = (A0-eta0-alpha)/2;
        
        if eta1*etam1 < 0
            error('The chosen unbalance is to big or negative intensity are chosen!');
        end
        

    case 3
        prompt = {'\Kappa','Initial point \tau_0'};
        dlg_title = 'Akhmediev Breather';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'1.414','-10'};
        IC_case=3;
        ABpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        
        
        Kappa0 = str2double( ABpar(1));
        T0AB = str2double( ABpar(2));
        a = (1-(Kappa0/2)^2)/2; 
        b = sqrt(8*a.*(1-2*a));
        
        
    case 4
        prompt = {'Soliton width','Soliton number'};
        dlg_title = 'N-soliton';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'1','1'};
        IC_case=4;
        Solpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        tau0 = str2double(Solpar(1));
        Kappa0 = 2*pi/tau0;
        Nsol = str2double( Solpar(2));
        waitfor(msgbox('The period length in x is chosen as the soliton width','Soliton'));
   		
		
	case 5
		IC_case=2;
        if nu0./lambda0 > 0
            warning('Plane waves are stable');
        end
        prompt = {'Total initial energy','Sideband fraction','Sideband unbalance','Relative phase (\phi_1+\phi_{-1})/2-\phi_0','\Omega/\Omega_Max'};
        dlg_title = 'Harmonically perturbed Plane Wave';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'1','1e-2','0e-3','pi/2','1'};
        
        PPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=PPWpar;

        A0 = str2double( PPWpar(1));
        eta0 = A0 - str2double(PPWpar(2));
        alpha = str2double(PPWpar(3));
        psi0 = (eval(char(PPWpar(4))));
        Kappa0 = 1/Lnl*sqrt(2)*str2double(PPWpar(5));
        windowwidth = 10*Lnl;
        eta1 = (A0-eta0+alpha)/2;
        etam1 = (A0-eta0-alpha)/2;
        
        if eta1*etam1 < 0
            error('The chosen unbalance is to big or negative intensity are chosen!');
        end
        
	case 6 
		IC_case=6;
    otherwise 
        error('No valid initial condition option');
end

% calculate the space window span
% Lx = 2*pi/Kappa0*Nper;
% Lx = 2*pi/k0*Nper;
% 
% fprintf('Ltank = %g\n',Lx*L0);

% save_params_names=prompt;%TO BE USED TO SAVE PARAMETERS TO FILE. 
%% definition of T lattice
% given the type of IC, we define omega0 and the time lattice
% omega0 is the characteristic  angular frequency dependent on the chosen initial
% conditions

hx = x(2)-x(1);
kappaxis = 2*pi.*linspace(-1/hx/2,1/hx/2,nx+1); kappaxis = kappaxis(1:end-1);
%FFT phase shifts
%FFT phase shifts
kx1 = linspace(0,nx/2 + 1,nx/2)';
kx2 = linspace(-nx/2,-1,nx/2)';
kx = ((2*pi/hx/nx)*[kx1; kx2]);
% phaselin = -1i/2.*kx.^2 + 1i/2*esteep0.*kx.^3 + (esteep0*Gamma.*kx + delta);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% field initialization
u = zeros(nx,Nt+1);
uspectrum = u;
saveflag = zeros (Nt+1,1);
N = saveflag;
P = N;

%% Initial conditions
switch excflag
    case 1
        u(:,1) = U0*(sqrt(eta0) + sqrt(variance).*randn(size(x)) + 1i.*sqrt(variance).*randn(size(x)));
        
    case 2
        u(:,1) = U0.*(sqrt(eta0) + sqrt(etam1).*exp(1i*Kappa0.*x).*exp(1i.*psi0) + sqrt(eta1).*exp(-1i*Kappa0.*x).*exp(1i.*psi0));

    case 3
        u(:,1)= ((1-4*a).*cosh(b*T0AB)+1i*b*sinh(b*T0AB)+sqrt(2*a).*cos(Kappa0.*x))./(sqrt(2*a).*cos(Kappa0.*x)-cosh(b.*T0AB));
    
    case 4
		u(:,1) = Nsol*(U0/tau0).*sech((U0/tau0)*sqrt(abs(nu0/2/lambda0)).*x);

%         u(:,1) = U0.*Nsol/tau0.*sech(x/tau0);
		
	case 5
	% Harmonically perturbed plane wave
		u(:,1) = U0.*(sqrt(eta0) + sqrt(etam1).*exp(1i*Kappa0.*x).*exp(1i.*psi0) + sqrt(eta1).*exp(-1i*Kappa0.*x).*exp(1i.*psi0));
	case 6 
		u(:,1) = U0;
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialization of computation
% u(:,1) = (u0);
ucurr = u(:,1);
uspectrum(:,1) = fftshift(ifft(ucurr,[],1),1);

pind = 1;
N(pind) = trapz(x,abs(ucurr).^2);
P(pind) = trapz(x,0.5i.*(fft(-1i.*kx.*ifft(ucurr)).*conj(ucurr)-fft(-1i.*kx.*ifft(conj(ucurr))).*ucurr));
t = 0;
ht = ht0;
pind = 2;
count = 0;
%%
figure()
subplot(1,2,1)
hold on
plot(x,abs(u(:,1)))
% plot(x,real(u(:,1).*cos(k0.*x')))
plot(x,real(u(:,1).*exp(1i.*k0.*x')));
hold off
title('initial condition')
xlabel('m')
subplot(1,2,2)
plot(kappaxis,abs(uspectrum(:,1)));
% xlim([-3*kas0,3*kas0])
xlabel('k')
% yscale('log')
%%
% plot_fourier_k_first(K,H,k0,k0,u,kappaxis,T,U0,Lx,xvarstart,xvarstop,30,'viridis')
	spectrum= fftshift(ifft(u(:,1),[],1),1);
	UdBth=-30;

	max_spec=max(abs(spectrum(:,1)));
	[ d, ix ] = min( abs( abs(spectrum(:,1)) - max_spec ) );%find the critical value index
	spectrum(ix,:)=0.5*spectrum(ix-1,:)+0.5*spectrum(ix+1,:);

	f=figure('name','Fourier first last','position',[20 20 500 500]);

    pos1 = [0.05 0.3 0.9 0.65];
    subplot(4,1,[1 3]);
    hold on
%     UdBth = -15;
    Usp = 10*log10(abs(spectrum.')*(1/(2*pi))/U0);
	Usp = Usp-max(Usp,[],"all");
    Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
	
	s2=plot(kappaxis/(2*pi),Usp );
	xlim([-3*kas0,3*kas0])
	ylabel('db')
	xlabel('k')
%% Computation
shoalflag=0;
figure;
filename = 'NLSAB.gif';
set(0,'defaulttextfontsize',14);
while t<Tmax
        err = tol*10;
        numberofrejstep = 0;
        while err>tol
% 			kh1=khfunsel(x+hx/2,kh0,khfin,xvarstart,xvarstop,xslope);
%             [~,k1,~,cg1,~,lambda1,nu1,alpha1,alpha41,beta1,gamma1,~,muintexp1,dythe_hilb1] = depthdependent_HONLSparameters_onoff(Omega,khfunsel(x+hx/2,kh0,khfin,xvarstart,xvarstop,xslope),FODflag,ON_OFF_coef);
%             kh2=khfunsel(x+hx,kh0,khfin,xvarstart,xvarstop,xslope);
% 			[~,k2,~,cg2,~,lambda2,nu2,alpha2,alpha42,beta2,gamma2,~,muintexp2,dythe_hilb2] = depthdependent_HONLSparameters_onoff(Omega,khfunsel(x+hx,kh0,khfin,xvarstart,xvarstop,xslope),FODflag,ON_OFF_coef);
% 			k1=k0;k2=k0;lambda1=lambda0;lambda2=lambda0;nu1=nu0;nu2=nu0;alpha1=alpha0;alpha2=alpha0;alpha41=alpha40;alpha42=alpha40;beta1=beta0;beta2=beta0;gamma1=gamma0;gamma2=gamma0;muintexp1=0;muintexp2=0;dythe_hilb1=dythe_hilb0;dythe_hilb2=dythe_hilb0;
% 			kh1=kh0;kh2=kh0;
			[ucurrnew,err]=DV4ORK34_spacelike_AD_simple(ht,u,kx,h0,lambda0,nu0,alpha0,alpha40,beta0,gamma0,dythe_hilb0,kh0);
% 			[ucurrnew,err] =  DV4ORK34_spacelike_AD(ht,ucurr,kx,H(1),lambda0,lambda1,lambda2,nu0,nu1,nu2,alpha0,alpha1,alpha2,alpha40,alpha41,alpha42,beta0,beta1,beta2,gamma0,gamma1,gamma2,muintexp0,muintexp1,muintexp2,dythe_hilb0,dythe_hilb1,dythe_hilb2,shoalflag,dis,kh0,kh1,kh2);			
            if (err<tol)
                t = t + ht;
                ucurr = ucurrnew;
                count = count + 1;
                step(count) = ht;
            else
                numberofrejstep =  numberofrejstep + 1;
                if numberofrejstep > 10
                    fprintf('10!\n');
                end
			end
			ht = min(max(0.5,min(2,.95*(tol./err).^.25)).*ht,Ht);
            if ht < htmin 
                error('Integration step too small');
            end
            if isnan(err)
                error('Diverges!');
            end
        end
    if isnan(ucurr)
        error('diverges!!!');
    end
%     k0 = k2;
%     lambda0 = lambda2;
%     muintexp0 = muintexp2;
%     nu0 = nu2;
%     cg0 = cg2;
%     alpha0 = alpha2;
%     alpha40 = alpha42;
%     beta0 = beta2;
%     gamma0 = gamma2;
%     dythe_hilb0=dythe_hilb2;
	
    %     hz0 = hz;
     if t>=T(pind) && saveflag(pind) == 0
        saveflag(pind) = 1;   
        N(pind) = trapz(x,abs(ucurr).^2);
        P(pind) = trapz(x,0.5i.*(fft(-1i.*kx.*ifft(ucurr)).*conj(ucurr)-fft(-1i.*kx.*ifft(conj(ucurr))).*ucurr) + 0*esteep0* abs(ucurr).^4);

        if max(abs(ucurr))*k2>0.4
            warning('Wave breaking probably occurs!');
        end

            u(:,pind) = ucurr;
            % computing spectrum
            uspectrum(:,pind) = fftshift(ifft(ucurr,[],1),1);
			T(pind) = t;
			timeevolution(x, t,kappaxis,u(:,pind),u(:,1),L0,T0,a0,k0,units);
		    pind = pind + 1;
			drawnow
		
        pind = pind + 1;
    end
    
end
%% recover the old variables (see B above)
% computing k spectrum
uksp = fftshift(ifft(u,[],1),1);
% omega spectrum
uspectrum = fftshift(fft(u,[],2),2);
freqax = linspace(-1/Ht/2,1/Ht/2,Nt+1) + f0;

u = u.*(ones(length(x),1)*(Cg.^0.5)).*Cg(1).^-0.5; %double check this
uspectrum =  uspectrum.*(ones(length(x),1)*(Cg.^0.5)).*Cg(1).^-0.5;%(double check this.. )
windowwidth = 10*Lnl;
xplot = abs(x)<=windowwidth;

switch Dimensionflag
    case 'Adimensional'
        mytlabel = '\tau';
        myxlabel = '\xi';
        myklabel = '\kappa';
        myflabel = 'f';
    case 'Dimensional'
        mytlabel = 't [s]';
        myxlabel = 'x [m]';
        myklabel = '\kappa [m^-1]';
        myflabel = 'f [Hz]';
end

%% export initial conditions for experiment on sydney
if Exportinit=='yes'
    sample_frequency = 100. ;%hz this is the sample frequency of the output. 
    initial_depth= H(1);
    pos_gau=import_pos_gau_for_input('pos_wavegauges.txt');
    pos_gau=table2array(pos_gau);
    %% make header
    Input_function=inputOptions{excflag};%manually choose the input function to be used
    Posible_functions=['Mod_instability','Soliton','Ak_Breather','Peregrine','Super_Gaussian'];
    input_signal=Factor*real(u(xplot,1).*exp(1i*(freq_dim*(2*pi)*x(xplot)/sqrt(g0)))');
    try
    figure('name','Output sample');
    xp=x(xplot)/sqrt(g0);
    [A1,x_sample] = resample(input_signal,xp,100);
    fprintf('sanity check: Wave maker sample frequency shoud be 0.01 \n sample frequency is:  %.3f \n ',t_sample(2)-t_sample(1))
    if A1(1)>0
        t0 = find(A1<0,1,'first');
        t1 = find(A1<0,1,'last');
    else
        t0 = find(A1>0,1,'first');
        t1 = find(A1>0,1,'last');
    end
    %borders at 0 (a tukey window might work also.)
    A2=[0 A1(t0:t1)' 0]';
    if t1==length(t_sample)
        t_sample(t1+1)=t_sample(t1);
    end
    t2= t_sample(t0-1:t1+1) ;
    M=[t2'-min(t2),A2];
    %% saving stuff
    [flm fullfolder]=save_filename(freq_dim,U0,esteep0,T0_dim);
    mkdir(fullfolder) ;
    filename_input=fullfile(fullfolder,strcat(flm,'_input.txt'));
    print_input_file2(filename_input,Input_function,sample_frequency,initial_depth,Factor,M);
    filename_wm=fullfile(fullfolder,strcat(flm,'_run_001.txt'));
    csvwrite(filename_wm,M) ;
    filename_pos=fullfile(fullfolder,strcat(flm,'_pos.txt'));
    print_pos_file(filename_pos,Input_function,sample_frequency,initial_depth,Factor,pos_gau);
    mkdir(fullfile(fullfolder,'simulation and input'))
	img_folder=fullfile(fullfolder,'simulation and input');
    filename_params=fullfile(fullfolder,strcat(flm,'_params.txt'));
    print_params_file(filename_params,Input_function,sample_frequency, save_params,save_params_names)
    figure('name','signal');
    title('Translated in time and borders at 0','FontSize',15);
    plot(t2'-min(t2),A2,'b');
    xlabel('T (s)','fontsize',14);
    ylabel('Amplitude (m)','fontsize',14);
    savefig(fullfile(img_folder,strcat(flm,'_input_signal')));
 	copyfile('button_dialog_temp_1.txt',fullfolder);
	copyfile('kh_dialog_temp.txt',fullfolder);
 	status = copyfile('default_coef_equation_temp.txt',fullfolder);
	
    %% figures
	if length(t_sample)==length(A1)
		figure('name','Resample')
		title('Resample from simulation','FontSize',15)
		hold on
		plot(t_sample,A1,'.r')
		plot(tp,input_signal,'-b')
		hold off
		xlabel('T (s)','fontsize',14);
		ylabel('Amplitude (m)','fontsize',14);
		savefig(fullfile(img_folder,strcat(flm,'_sample_check')));
	end
%%
	figure('name','signal')
    title('Translated in time and borders at 0','FontSize',15)
    subplot(211);
    l1=plot(t2'-min(t2),A2,'b');
    set(l1,'LineWidth',.8)
    xlabel('T (s)','fontsize',14);
    ylabel('Amplitude (m)','fontsize',14);
    subplot(212);
    sig_ff=ifft(A2);
    L=length(sig_ff);
    P2 = abs(sig_ff/L);
    P1 = P2(1:L/2+1);
    P1(2:end-1) = 2*P1(2:end-1);
    fa=100;
%Define the frequency domain f and plot the single-sided amplitude spectrum P1. The amplitudes are not exactly at 0.7 and 1, as expected, because of the added noise.
% On average, longer signals produce better frequency approximations.
    f = fa*(0:(L/2))/L;
    pl_1=plot(f,P1);
    set(pl_1,'LineWidth',1)
    xlim([0,3]); 
    savefig(fullfile(img_folder,strcat(flm,'_input_signal2')));
	end
end
%% Generic plots
f = plot_parameters_1(T,Cg,Lambda,cg0,Nu);
if Exportinit=='yes'
        savefig(fullfile(img_folder,strcat(flm,'_params')));
end 
%% Conservations plot.
f = plot_conservations(T,N,P);


%% Data processing: extracting sidebands for three-wave phase-space
if any(IC_case==[1,2,3])
	IAS = find(kappaxis<=Kappa0+.1 & kappaxis>=Kappa0-.1); IAS = IAS(round(length(IAS)/2));
IS = find(kappaxis<=-Kappa0+.1 & kappaxis>=-Kappa0-.1); IS = IS(round(length(IS)/2));
IAS2 = find(kappaxis<=2*Kappa0+.1 & kappaxis>=2*Kappa0-.1); IAS2 = IAS2(round(length(IAS2)/2));
IS2 = find(kappaxis<=-2*Kappa0+.1 & kappaxis>=-2*Kappa0-.1); IS2 = IS2(round(length(IS2)/2));
IAS3 = find(kappaxis<=3*Kappa0+.1 & kappaxis>=3*Kappa0-.1); IAS3 = IAS3(round(length(IAS2)/2));
IS3 = find(kappaxis<=-3*Kappa0+.1 & kappaxis>=-3*Kappa0-.1); IS3 = IS3(round(length(IS2)/2));


utrunc = uksp([nx/2+1,IS,IAS,IS2,IAS2,IS3,IAS3],:);
Eta0 = abs(utrunc(1,:)).^2;
Eta1 = abs(utrunc(3,:)).^2;
Etam1 = abs(utrunc(2,:)).^2;
Alpha = Eta1 - Etam1;
Utr = Eta0 + Eta1 + Etam1;
Phi = unwrap(angle(utrunc(3,:)))+unwrap(angle(utrunc(2,:))) - 2*unwrap(angle(utrunc(1,:)));

% plotting normalized frequency components 
figure;
subplot(211)
plot(T*L0, Eta0, T*T0, Eta1,'g-.', T*T0, Etam1,'r-.', T*T0, abs(utrunc([4,6],:)).^2,'g-.', T*T0, abs(utrunc([5,7],:)).^2,'r-.',T*T0, Utr,'k:',T*T0,N./N(1),'linewidth',1.2);
legend('\eta_0','\eta_1','\eta_{-1}','\eta_{-2}','\eta_{-3}','\eta_{2}','\eta_{3}','\eta+\eta_1+\eta_{-1}','N/N(0)');
xlabel(mytlabel ,'fontsize',15);
set(gca,'fontsize',14)
xlim([0,Tmax*T0]);

subplot(223);
plot((Eta1+Etam1).*cos(Phi/2), (Eta1+Etam1).*sin(Phi/2),(Eta1(1)+Etam1(1)).*cos(Phi(1)/2), (Eta1(1)+Etam1(1)).*sin(Phi(1)/2),'diamond');
set(gca,'fontsize',14);
axis equal
xlabel('\eta'' cos \psi/2','fontsize',16);
ylabel('\eta'' sin \psi/2','fontsize',16);

subplot(224);
plot(T*L0, Kappa0*Alpha, ...
    T*L0,P./N,'--k','linewidth',1.2);
legend('\Omega\alpha','P/N');
xlabel(mytlabel,'fontsize',15);
set(gca,'fontsize',14);
xlim([0,Tmax*L0]); ylim([-.5,.5]);
end
%%

figure;
UdBth = -40;
Uksp = 20*log10(abs(uksp.')*a0);
Uksp = (Uksp>UdBth).*(Uksp-UdBth) +  UdBth;
imagesc(kappaxis/L0+2*pi*k0,T*T0,Uksp);
set(gca,'YDir','normal','FontSize',14);
xlim([-10*Kappa0+2*pi*k0,10*Kappa0+2*pi*k0]);
xlabel(myklabel,'fontsize',16);
ylabel(mytlabel,'fontsize',16);
% shading interp;
colormap hot;
colorbar;


% spectrum map
figure;
UdBth = -50;
Usp = 20*log10(abs(uspectrum)*a0);
Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
imagesc(freqax,x,Uksp);
set(gca,'YDir','normal','FontSize',14);
xlim([f0-1/T0,f0+1/T0]);
xlabel(myflabel,'fontsize',16);
ylabel(myxlabel,'fontsize',16);
% shading interp;
colormap hot;
colorbar;

% absolute value evolution
figure;
imagesc(x(abs(x)<=2*pi/Kappa0)*L0,T*T0,abs(u(abs(x)<=2*pi/Kappa0,:).').^2*a0^2);
set(gca,'YDir','normal','FontSize',14);
xlabel(myxlabel,'fontsize',16);
ylabel(mytlabel,'fontsize',16);
shading interp;
colormap hot;

% "conserved" quantities
figure;
subplot(311)
plot(T*T0,N./N(1),'linewidth',1.5);
set(gca,'FontSize',14);
ylabel('N/N(0)','fontsize',16);
subplot(312);
plot(T*T0,P./P(1),'linewidth',1.5);
set(gca,'FontSize',14);
xlabel(myxlabel,'fontsize',16);
ylabel('P/P(0)','fontsize',16);
subplot(313);
plot(T(2:end)*T0,diff(P./N),'linewidth',1.5);
set(gca,'FontSize',14);
xlabel(myxlabel,'fontsize',16);
ylabel('d(N/N0)/dx','fontsize',16);
suptitle('Conservation of momenta');

% 	figure('name','regime and decomp');
% 	axes('Position',[.12,.58,.74, .36]);
% 	plot(X, Eta0, X, Eta1,'g-.', X, Etam1,'r-.', ...
% 		X, Eta2,'g--', X , Etam2,'r--', X, Utr,'k:','linewidth',1.2);
% 	% legend('\eta_0','\eta_1','\eta_{-1}','\eta_{-2}','\eta_{-3}','\eta_{2}','\eta+\eta_1+\eta_{-1}');
% 	xlim([0,Tmax]);
% 	set(gca,'FontSize',14,'XTickLabel','');
% 	ylabel('\eta_n','FontSize',15)
% 	% text(.15,.8,'(a)');
% 
% 	%%
% 	axes('Position',[.12,.15,.74, .36]);
% 	[phax,phlin,nl] = plotyy(X, Psi/2/pi, ...
% 		X, omega0./sqrt(abs(Nu./Lambda))./U0);
% 	set(phlin,'linewidth',1.5,'color','b');
% 	set(nl,'linewidth',1.5,'linestyle','-','color','r');
% 	hold(phax(1),'on');
% 	% plot(phax(2),X,sqrt(2).*ones(size(X)),':','linewidth',1.2)
% 	plot(phax(1),X,ones(size(X)),':b','linewidth',1.2)
% 	% legend('\Omega\alpha','P/N');
% 	xlabel('X','fontsize',15);
% 	xlim([0,Tmax]); 
% 	% ylim([-.5,.5]);grid;
% 	set(phax(1),'FontSize',14,'xlim',[0,Tmax],'Ycolor','b');
% 	set(phax(2),'FontSize',14,'xlim',[0,Tmax],'Ycolor','r');
% 	ylabel(phax(1),'\psi/\pi','FontSize',15);
% 	ylabel(phax(2),'\nu/\lambda','FontSize',15);
% 	% text(.15,1.35,'(b)');
% 	hold(phax(1),'off');
% 	if Exportinit=='yes'
% 	   savefig(fullfile(img_folder,strcat(flm,'_regime')));
% 	end
% 
% 	%phase plane plots
% 	figure('name','3w space 1');
% 	plot(Psi/2/pi,(Eta1+Etam1)./ Utr,Psi(1)/2/pi,(Eta1(1)+Etam1(1))./Utr(1),'rx',Psi/2/pi,(Eta0)./ Utr,'--',Psi(1)/2/pi,Eta0(1)./Utr(1),'rx','linewidth',1);
% 	set(gca,'FontSize',14);
% 	xlabel('\psi/\pi','Fontsize',16);
% 	ylabel('\eta','Fontsize',16);
% 	if Exportinit=='yes'
% 		savefig(fullfile(img_folder,strcat(flm,'_3W_space1')));
% 	end 
% 
% 	figure('name','3w space 2');
% 	plot((Eta1+Etam1)./ Utr.*cos(Psi/2),(Eta1+Etam1)./ Utr.*sin(Psi/2),(Eta1(1)+Etam1(1))/Utr(1)*cos(Psi(1)/2),(Eta1(1)+Etam1(1))/Utr(1)*sin(Psi(1)/2),'rx','linewidth',1);
% 	set(gca,'FontSize',14);
% 	xlabel('\eta cos(\psi)','Fontsize',16);
% 	ylabel('\eta sin(\psi)','Fontsize',16);
% 	if Exportinit=='yes'
% 		savefig(fullfile(img_folder,strcat(flm,'_3W_space2')));
% 	end 
% 
% 	% Akhmedievstyle phaseplane plot (full NLS minus global phase)
% 	if IC_case==3
% 		Ucomplexplane = u/U0.*exp(-1i*ones(length(t),1)*unwrap(angle(utrunc(1,:))));
% 		figure('name','Akhmedievstyle phaseplane');
% 		plot(real(Ucomplexplane(nx/2+1,:)),imag(Ucomplexplane(nx/2+1,:)),real(Ucomplexplane(nx/2+1,1)),imag(Ucomplexplane(nx/2+1,1)),'rx','linewidth',1);
% 		set(gca,'FontSize',14);
% 		xlabel('Re B(t=0)','Fontsize',16);
% 		ylabel('Im B(t=0)','Fontsize',16);
% 		if Exportinit=='yes'
% 			savefig(fullfile(img_folder,strcat(flm,'_Akhmediev_space')));
% 		end 
% 	end	
% 	%% 3 wave from data (not done)	
% % 	three_wave_getsideband(u,freq_dim,t)
% 	
% end
% 
% switch Dimensionflag
%     case 1    
%     %%
%    
%  %%  Surface 2d plot with bathymetry   
% % 	cmap='viridis';
% 	cmap='viridis5perc';
% 
% 	plot_Amplitude_Evolution_wbath(K,H,X,t,u,xplot,Tmax,xvarstart,xvarstop,cmap);
%     if Exportinit=='yes'
%        savefig(fullfile(img_folder,strcat(flm,'_2d_evol')));
%        saveas(gcf,fullfile(fullfolder,strcat(flm,'_2d_evol','.jpeg')));
% 	end
% 	
% %%  Fourier 2d plot with bathymetry
% 	plot_fourier_bathm(K,H,Omega,omega0,uspectrum,omegaxis,X,U0,Tmax,xvarstart,xvarstop,'inferno')
%     if Exportinit=='yes'
% 		savefig(fullfile(img_folder,strcat(flm,'_2d_fourier')));
% 	end
% 	
% 	%% 3d envelope and steepness    
%     f=plot_3d_envelope(u,t,X,U0,xplot);
% %%
%     f=plot_3d_steepness(u,t,X,K,xplot);
%     %%
% 	if any(IC_case==[1,2,3])
% 		nu_tilde=Nu*Cg(1)./Cg;
% 		alpha_eta=-1.-Lambda.*((omega0)^2)./(nu_tilde.*U0^2);
% 		eta_MI=2/7.*(1.-alpha_eta);
% 		figure('name','alpha and eta');
% 		plot(X,alpha_eta,'k');
% 		ylabel('\alpha','fontsize',16);
% 		ax2 = gca;
% 		yyaxis right
% 		plot(X,eta_MI);
% 		ylabel('\eta MI','fontsize',16);
% 	end
%     %% tank plot
% 	if khfin~=kh0
% 		f=figure('name','bathymetry','position',[20 20 600 200]);
% 		hold on
% 		ax=gca;
% 		plot(X,-H);
% 		plot(X,-1*ones(size(X)));    
% 		plot(X,-H(1)*ones(size(X)),':k');
% 		plot(X,0*ones(size(X)),'b');
% 		ang=atan((-H(length(H)-1)+H(1))/(xvarstop-xvarstart));
% 		angles=linspace(0,ang,25);
% 		amp=(xvarstop-xvarstart)*0.5;
% 		if max(X)>30
% 		   plot([30 30],[0 -1],':k')
% 		   if Exportinit=='yes'
% 			   for j=1:length(pos_gau)
% 			   plot([pos_gau(j) pos_gau(j)],[0 -min(H)],'-.k')
% 			   end
% 			end
% 		end
% 		plot(amp*cos(angles)+xvarstart,amp*sin(angles)-H(1),'r' )
% 		hold off
% 		try
% 		if max(X)>30
% 		   set(ax,'XLim',[0. Tmax],'xtick',sort([0,xvarstart,xvarstop,30,Tmax]) ) ;
% 		else
% 			set(ax,'XLim',[0. Tmax],'xtick',sort([0,xvarstart,xvarstop,Tmax]) ) ;
% 		end
% 		end
% 		ylabel('h [m]','fontsize',14);
% 		xlabel('x [m]','fontsize',14);
% 		angle=num2str(rad2deg(ang));
% 		legend({strcat('angle= ',angle) },'Location','southwest') ;
% 		try
% 		ax.YAxis.TickValues=sort([-1,min(-H),max(-H)]);
% 		end
% 		ax.YAxis.Exponent=-2;
% 		if Exportinit=='yes'
% 			savefig(strcat(img_folder,'\',flm,'_bathm'))
% 			filename_bathm=strcat(fullfolder,'\',flm,'_bathm.txt');
% 			M=[X' -H'];
% 			print_bathym_file2(filename_bathm,Input_function,sample_frequency,initial_depth,Factor,M)
% 		end
% 	end
% %%   
%   if Exportinit=='yes'
%     pos_error=zeros(length(pos_gau),1);
% 	figure('name','aprox signals');
% 	title('Simulated gauge signals','FontSize',15)
% 	plot_factor=20;
% 	hold on
% 	for j=1:length(pos_gau)
% 	pos_gau(j);
% 	[ d, ix ] = min( abs( X - pos_gau(j) ) );%find the critical value index
% 	if j==1
% 		ix1=ix;
% 	end
% 	X(ix);
% 	pos_error(j)=X(ix)-pos_gau(j);
% 	ix0 = find(xplot==1,1,'first');
% 	plot(t/sqrt(g0)-t(ix0)/sqrt(g0),abs(u(:,ix))*plot_factor+X(ix),'-b');    
% 	plot(t/sqrt(g0)-t(ix0)/sqrt(g0),...
% 		real(u(:,ix)'.*exp(1i*2*pi*freq_dim.*(t/sqrt(g0)-t(ix0)/sqrt(g0)))).*plot_factor+X(ix),...
% 	'-r');   
% 	end  
% 	hold off 
% 	set(gca,'YMinorTick','on')
% 	xlim([0,30]);
% 	ylim([0,30]);
% 	grid minor
% 	ylabel('Distance to wave maker (m)','Interpreter','latex','fontsize',14);
% 	xlabel('${t-x/C_g}$ (s)','Interpreter','latex','fontsize',14);
%     savefig(fullfile(img_folder,strcat(flm,'_sim_gauges')));
%     %%
%     [ d, ix ] = min( abs( t/sqrt(g0) - Out_time ) );
%     fprintf( 'Usanity check: \n output time: %.2f is lower than half total time %.2f \n', Out_time, t(end)/sqrt(g0) );
%     middle=round(length(t)*0.5);
%     ix=round(abs(middle-ix)*0.5);
%       j=3;
%     pos_gau(j);
%     [ d, ix2 ] = min( abs( X - pos_gau(j) ) );%find the critical value index
%     X(ix2);
%     input_signal=Factor*real(u(middle-ix:middle+ix,ix2).*exp(1i*(freq_dim*(2*pi)*t(middle-ix:middle+ix)/sqrt(g0)))');
%     try
% 		figure('name','Output sample');
% 		tp=t(middle-ix:middle+ix)/sqrt(g0);
% 		[A1,t_sample_try] = resample(input_signal,tp,100);
% 		t_sample=zeros(length(A1));
% 		t_sample=t_sample_try(1:length(A1));
% 		fprintf('sanity check: Wave maker sample frequency shoud be 0.01 \n sample frequency is:  %.3f \n ',t_sample(2)-t_sample(1))
% 	%     A1=lowpass(A1,1.95);
% 		if A1(1)>0
% 			t0 = find(A1<0,1,'first');
% 			t1 = find(A1<0,1,'last');
% 		else
% 			t0 = find(A1>0,1,'first');
% 			t1 = find(A1>0,1,'last');
% 		end
% 		%borders at 0 (a tukey window might work also.)
% 		A2=[0 A1(t0:t1)' 0]';
% 		if t1==length(t_sample)
% 			t_sample(t1+1)=t_sample(t1);
% 		end
% 		t2= t_sample(t0-1:t1+1) ;
% 		M=[t2'-min(t2),A2];
% 	end
% %     csvwrite(filename_wm3,M) ;
% 
%     %%
%     stokes_amp=0.5*esteep0^2/k0;
%     pos_error=zeros(length(pos_gau),1);
%     figure('name','aprox signals stks');
%     title('Simulated gauge signals with stokes amplification','FontSize',15)
%     plot_factor=20;  
%     wmod=2*pi*freq_dim;
%     stk_sig_env=zeros(length(t),length(pos_gau));
%     stk_sig=zeros(length(t),length(pos_gau));
% 
%     hold on
%     for j=1:length(pos_gau)
%     pos_gau(j);
%     [ d, ix ] = min( abs( X - pos_gau(j) ) );%find the critical value index
%     if j==1
%         ix1=ix;
%     end
%     X(ix);
%     pos_error(j)=X(ix)-pos_gau(j);
%     ix0 = find(xplot==1,1,'first');
%     u2=u(:,ix).*u(:,ix);
%     stk_sig_env(:,j)=abs(u(:,ix)'.*exp(1i*wmod.*(t/sqrt(g0)-t(ix0)/sqrt(g0)))+...
%         0.5*k0*u2'.*exp(2*1i*wmod.*(t/sqrt(g0)-t(ix0)/sqrt(g0))));
%     stk_sig(:,j)=real(u(:,ix)'.*exp(1i*wmod.*(t/sqrt(g0)-t(ix0)/sqrt(g0)))+...
%         0.5*k0*u2'.*exp(2*1i*wmod.*(t/sqrt(g0)-t(ix0)/sqrt(g0))));
%       
%     l1=plot(t/sqrt(g0)-t(ix0)/sqrt(g0),...
%         stk_sig_env(:,j)*plot_factor+X(ix),...
%         '--k');   %'color',[.7 .2 .0]
%     set(l1,'LineWidth',.7)
%     
%     l2=plot(t/sqrt(g0)-t(ix0)/sqrt(g0),...
%         stk_sig(:,j).*plot_factor+X(ix),...
%     '-r');   
%     set(l2,'LineWidth',.6)
% 
%     end  
%     hold off 
%     set(gca,'YMinorTick','on')
%     xlim([0,30]);
%     ylim([0,30]);
%     grid minor
%     ylabel('Distance to wave maker (m)','Interpreter','latex','fontsize',14);
%     xlabel('${t-x/C_g}$ (s)','Interpreter','latex','fontsize',14);
%     savefig(fullfile(img_folder,strcat(flm,'_sim_gauges_stokes')));
% 
% %     filename_bathm=strcat(fullfolder,'\',flm,'_bathm.txt');
% %         M=[X' -H'];
% %         print_bathym_file2(filename_bathm,Input_function,sample_frequency,initial_depth,Factor,M)
% 
%  %%
%  try
% 	figure('name','Fourier signals')
% 	stokes_amp=0.5*esteep0^2/k0;
%     pos_error=zeros(length(pos_gau),1);
%     title('Simulated Fourier gauge signals \n with stokes amplification','FontSize',15)
%     plot_factor=20;  
%     wmod=2*pi*freq_dim;
%     hold on
%     for j=1:length(pos_gau)
%     pos_gau(j);
%     [ d, ix ] = min( abs( X - pos_gau(j) ) );%find the critical value index
%     if j==1                                                                                                                                                                                                                                                                                                                                                                                                    
%         ix1=ix;
%     end
%     X(ix);
%     pos_error(j)=X(ix)-pos_gau(j);
%     ix0 = find(xplot==1,1,'first');
%     fa=1.127/((t(2)-t(1)));%why is it not right???????
%     f = fa*(0:(L/2))/L;
% %Define the frequency domain f and plot the single-sided amplitude spectrum P1. The amplitudes are not exactly at 0.7 and 1, as expected, because of the added noise.
% % On average, longer signals produce better frequency approximations.
%     P2 = abs(ifft(stk_sig_env(:,j))/L);
%     P1 = P2(1:L/2+1);
%     P1(2:end-1) = 2*P1(2:end-1);   
%     P3= abs(ifft(stk_sig(:,j)) /L ) ;  
%     P4 = P3(1:L/2+1);
%     P4(2:end-1) = 2*P4(2:end-1);
%     if j==1
%       norm_four=max(P4);
%     end
% %     l1=plot(f*sqrt(g0),1.5*P1/max(P1)+X(ix),'--r');
%     l2=plot(f,7*abs(P4)/norm_four+pos_gau(j),'-r');
%     set(l2,'LineWidth',.8)
%     end  
% 
% 	hold off
% 	xlim([0,4])
% 	% title('Evolution of the freq.','Interpreter','latex')
% 	xlabel('f (Hz)','Interpreter','latex')    
% 	savefig(fullfile(img_folder,strcat(flm,'_fourier_sims_stk')));
%  end
% 
%     %%
% 	plot_simulated_gauge_fourier_stokes(k0,esteep0,freq_dim,omegaxis,pos_gau,X,t,L,u,xplot)
% 	savefig(fullfile(img_folder,strcat(flm,'_fourier_sims')));
% 
% 
%     end
%     
% %%
%     case 2
% 
%    
% %%
%     figure;
%     Pos = [.14,.15,.79,.72];
%     axes('Position',Pos);
%     imagesc(t(abs(t)<=windowwidth),X,abs(u(xplot,:).')/U0);
%     hold on;
%     [~,nucross] = min(abs(H.*K-1.363));
%     plot(t(xplot),xvarstart*ones(size(t(xplot))),'k:',t(xplot),xvarstop*ones(size(t(xplot))),'k:',t(xplot),X(nucross)*ones(size(t(xplot))),'m-.','Linewidth',2);
%     set(gca,'YDir','normal','FontSize',14);
%     xlabel('\tau [m^{1/2}]','fontsize',16);
%     ylabel('\xi [m]','fontsize',16);
%     %shading interp; colorbar;
%     Pos = get(gca,'Position');
%     Yl = get(gca,'ylim');
%     % overlayed axis with kh changes
%     axes('Position',Pos);
%     plot(H.*K,X,'r--','linewidth',2);
%     xlabel('kh','fontsize',16);
%     set(gca,'XAxisLocation','top','GridAlpha',1,'YTickLabel','','Fontsize',14,'Ylim',Yl,'Color','none')
%    
%     f=figure('position',[20 20 500 500]);
%     [ d, ix ] = min( abs( K.*H - crit ) );%find the critical value index
%     pos1 = [0.05 0.3 0.9 0.65];
%     subplot(4,1,[1 3]);
%     % hold on 
%     imagesc(X,t(xplot),abs(u(xplot,:)) );
%      set(gca,'XDir','normal','FontSize',14,'xtick',[]);
%      ylabel('\tau [m^{1/2}] ','fontsize',16);
%     % plot(-X0.*ones(max(size(t(xplot)/sqrt(g0)))),t(xplot)/sqrt(g0),'--g')
%     % plot(xvarstart.*ones(max(size(t(xplot)/sqrt(g0)))),t(xplot)/sqrt(g0),'color',[0.87,0.74,0.53],'LineStyle','--')
%     % plot(xvarstop.*ones(max(size(t(xplot)/sqrt(g0)))),t(xplot)/sqrt(g0),'color',[0.87,0.74,0.53],'LineStyle','--')
%     % if K(ix)*H(ix)<(crit+0.02) & K(ix)*H(ix)>(crit-0.02);
%     %      plot(X(ix).*ones(max(size(t(xplot)/sqrt(g0)))),t(xplot)/sqrt(g0),'r','LineStyle',':')
%     % end
%     % hold off
%     shading interp;
%     hc=colorbar;
%     title(hc,'|B|');
%     ax1 = gca;
%     ax1_pos = ax1.OuterPosition ;
%     subplot(4,1,4);
%     ax2 = gca;
%     ax2_pos = ax2.OuterPosition;
%     ax2_pos =  [ ax1_pos(1)  ax2_pos(2)  ax1_pos(3)  ax2_pos(4)];
%     set(ax2,'OuterPosition',ax2_pos);
%     yline=H.*K;
%     yline(end) = NaN;
%     c = yline;
%     patch(X,yline,c,'EdgeColor','interp','LineWidth',1.2,'MarkerFaceColor','flat');
%     % colorbar;
%     xlabel('\xi [m]','fontsize',16);
%     ylabel('hk','fontsize',16);
%     set(gca,'XDir','normal','FontSize',14,'Ylim',[0.85*min(H.*K), 1.15*max(H.*K)],'ytick',[min(H.*K)  max(H.*K)*1.0000001],'XLim',[0. Tmax]);
%     ytickformat('%.1f');
%           
%     figure;
%     meshc(t(xplot),X,abs(u(xplot,:).')/U0);
%     set(gca,'YDir','normal','FontSize',14);
%     xlabel('T [m^{1/2}]','fontsize',16);
%     ylabel('X [m]','fontsize',16);
%     zlabel('|B|/B_0','fontsize',16);
%     % shading interp;
%     % colormap hot;
% 
%     figure;
%     meshc(t(xplot),X,abs(u(xplot,:).').*(K.'*ones(size(t(xplot)))));
%     set(gca,'YDir','normal','FontSize',14);
%     xlabel('T [m^{1/2}]','fontsize',16);
%     ylabel('X [m]','fontsize',16);
%     zlabel('Steepness','fontsize',16);
%     % shading interp;
%     colormap hot;
% 
%     % spectrum map
%     figure;
%     UdBth = -60;
%     Usp = 20*log10(abs(uspectrum.')/U0);
%     Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
%     imagesc(omegaxis,X,Usp);
%     hold on;
%     plot(omegaxis,xvarstart*ones(size(omegaxis)),'k:',omegaxis,xvarstop*ones(size(omegaxis)),'k:','Linewidth',2);
%     set(gca,'YDir','normal','FontSize',14);
%     xlim([-4*omega0,4*omega0]);
%     xlabel('\omega [m^{-1/2}]','fontsize',16);
%     ylabel('X [m]','fontsize',16);
%     % shading interp;
%     % colormap hot;
%     colorbar;
%     Pos = get(gca,'Position');
%     Yl = get(gca,'ylim');
%     axes('Position',Pos);
%     plot(H.*K,X,'r--','linewidth',2);
%     xlabel('hk','fontsize',16);
%     set(gca,'XAxisLocation','top','GridAlpha',1,'YTickLabel','','Fontsize',14,'Ylim',Yl,'Color','none')
% 
% end
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% %% benchmark plots
% if any(IC_case==[13])
%     f=figure('name','Fourier','position',[20 20 500 500]);
%     hold on
%     UdBth = -15;
%     Usp = 10*log10(abs(uspectrum.').*sqrt(length(u(:,1)).*2)*sqrt(g0)*(1/(2*pi)));
% % 	    Usp = 10*log10(abs(uspectrum.')./max(uspectrum,[],"all"));
% % 	    Usp = 20*log10(abs(uspectrum.').*sqrt(length(u(:,1)).*2)*sqrt(g0)*(1/(2*pi))./U0);
% %     Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
%     Usp = Usp-max(Usp,[],"all");
% 	Usp = (Usp>UdBth).*(Usp-UdBth)+UdBth;
% 
% 	%     imagesc(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,abs(uspectrum')*sqrt(g0)*(1/(2*pi))/U0);
%     imagesc(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,Usp);
% 
% 	set(gca,'XDir','normal','FontSize',14);
% %     ylim([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)]);
%     xlim([1,3.5]);
% 	ylim([X(1),X(end)])
%     xlabel('f [Hz]','fontsize',16);
%     ylabel('x [m]','fontsize',16);
%     ytickformat('%.1f');
% 	colorMap = jet(256);
% 	colormap(colorMap);   % Apply the colormap
% 	hold off
% 	colorbar()
% 	
% 	
% 			L0=2*pi./k0;
% 
% 			f=figure('position',[20 20 500 500]);
% 		s=pcolor(t(xplot)./sqrt(g0),fliplr(X),flipud(abs(u(xplot,:)')));
% 		set(s, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
% 	% 	set(s,'ytick',[0 20 ] ,'xtick',[ 0 min(H.*K)  max(H.*K)*1.0000001 ],'xticklabel',{'hk:' min(H.*K)  max(H.*K)})
% 	% 	set(gca,'ytick',[0 20 40 60],'yticklabel',{0 20 40 60},'xtick',[-16 -8 0 8 16],'xticklabel',{-16 -8 0 8 16})
% 		hold on
% 		plot([t(1)./sqrt(g0) t(end)./sqrt(g0)],[4 4],'--g')
% 		hold off
% 		 xlabel('t[s] ','fontsize',16); 
% 		 ylabel('x[m]  ','fontsize',16);
% 		 xlim([-10,10])
% 		 ylim([0,10])
% 		colorMap = jet(256);
% %  		colorMap = brighten(colorMap,-0.25);
% 		colormap(colorMap);   % Apply the colormap
% 		cb=colorbar();
% 		ylabel(cb, '|U|')
% 		caxis([0 8*10^(-3)])
% end
% 	
% 		%%
% 	if IC_case==11
% 		f=plot_zhang(k0,t,xplot,X,u,U0,T0);
% 		if Exportinit=='yes'
% 			savefig(fullfile(img_folder,strcat(flm,'_zhang_paper_plot')));	
% 		end
% 	end
% %% Plots for solitions
% 	if any(IC_case==[5,14])
% 		f=plot_soliton_benchmark(K,H,Omega,omega0,uspectrum,omegaxis,X,U0,Tmax,xvarstart,xvarstop,'jet')
% 		if Exportinit=='yes'
% 			savefig(fullfile(img_folder,strcat(flm,'_soliton_benchmark_fourier')));	
% 			saveas(gcf,fullfile(fullfolder,strcat(flm,'_soliton_benchmark_fourier','.jpeg')));
% 		end
% 		if Nsol==4
% 			f=plot_soliton_benchmark_N4(K,H,X,t,u,xplot,Tmax,xvarstart,xvarstop,'jet',U0)
% 			if Exportinit=='yes'
% 				savefig(fullfile(img_folder,strcat(flm,'_soliton_benchmark_N4')));	
% 				saveas(gcf,fullfile(fullfolder,strcat(flm,'_soliton_benchmark_N4','.jpeg')));
% 			end
% 			f=plot_fourier_solitonN4(K,H,Omega,omega0,uspectrum,omegaxis,X,U0,Tmax,xvarstart,xvarstop,cmap);
% 			if Exportinit=='yes'
% 				savefig(fullfile(img_folder,strcat(flm,'_soliton_benchmark_fourier_N4')));	
% 				saveas(gcf,fullfile(fullfolder,strcat(flm,'_soliton_benchmark_fourier_N4','.jpeg')));
% 			end	
% 		end
% 	end
% %% Plots for Gaussian/Supergaussian
% 	if any(IC_case==[7,13])
% % 		        Bback = str2double( Pertpar(2));
% %         Bpert = str2double( Pertpar(3));
% 		f = plot_Amplitude_Evolution_first_last(K,H,X,Bback,Bpert,U0,t,u,xplot,Tmax,xvarstart,xvarstop,cmap);
% 		if Exportinit=='yes'
% 			savefig(fullfile(img_folder,strcat(flm,'envelope_first_last')));	
% 			saveas(gcf,fullfile(fullfolder,strcat(flm,'_envelope_first_last','.jpeg')));
% 		end
% 	end
% 		%%
% 	if any(IC_case==[7])
% 		UdBth=-25;
% 		f = plot_fourier_first_last(K,H,Omega,omega0,u,omegaxis,X,U0,Tmax,xvarstart,xvarstop,UdBth,'inferno');
% 		if Exportinit=='yes'
% 			savefig(fullfile(img_folder,strcat(flm,'_fou_first_last')));	
% 		end
% 	end
% 	%%
% 	if any(IC_case==[7])
% 	cmap=colorcet('D2'); %D1-D11 Perceptually linear divergent colormaps. I like D6 or D2, since it has none of the usuall colors. 
% 	f=plot_chirp_Evolution_first_last(K,H,X,Bback,Bpert,U0,t,u,xplot,Tmax,xvarstart,xvarstop,cmap);
% 	if Exportinit=='yes'
% 			savefig(fullfile(img_folder,strcat(flm,'_chirp_first_last')));	
% 		end
% 	end
% 	%%
% % 	FOD=0;
% % 	if any(IC_case==[7])
% % 		cmap='viridis';
% % 		f=plot_V_shock(K,H,X,Bback,Bpert,U0,t,u,xplot,Tmax,xvarstart,xvarstop,cmap,Omega,kh0,FOD,ON_OFF_coef);
% % 		if Exportinit=='yes'
% % 			savefig(fullfile(img_folder,strcat(flm,'_shock_speed')));	
% % 		end
% % 	end
% % 		
% 	%%
% 	if IC_case==7
% 		fig_fou_sanscarrier = plot_fourier_sanscarrier(u,omegaxis,Omega,X);
% 		if Exportinit=='yes'
% 			savefig(fullfile(img_folder,strcat(flm,'_fourier_sans_carrier')));	
% 		end
% 		
% 		%%
% 	plot_contrast_shock(U0,Bback,u,X);
% 	if Exportinit=='yes'
% 		savefig(fullfile(img_folder,strcat(flm,'_contrast')));	
% 	end
% 		
% 	end
	
	
	

%%	old plots
%  figure;
%     [ d, ix ] = min( abs( K.*H - crit ) );%find the critical value index
%     hold on
%     imagesc(t(xplot)/sqrt(g0),X,abs(u(xplot,:).'));
%     set(gca,'YDir','normal','FontSize',14);
%     xlabel('\tau [s] ','fontsize',16);
%     ylabel('\xi [m]','fontsize',16);
%     % if IC=='Peregrine Soliton'
%     %     caxis([0 U0*3]);
%     % end
%     shading interp;
%     colorbar
%     % colormap jet;
%     %colormap hot;
% %     plot(t(xplot)/sqrt(g0),-X0.*ones(max(size(t(xplot)/sqrt(g0)))),'--g')
%     plot(t(xplot)/sqrt(g0),xvarstart.*ones(max(size(t(xplot)/sqrt(g0)))),'color',[0.87,0.74,0.53],'LineStyle','--')
%     plot(t(xplot)/sqrt(g0),xvarstop.*ones(max(size(t(xplot)/sqrt(g0)))),'color',[0.87,0.74,0.53],'LineStyle','--')
%     if K(ix)*H(ix)<(crit+0.02) & K(ix)*H(ix)>(crit-0.02);
%         plot(t(xplot)/sqrt(g0),X(ix).*ones(max(size(t(xplot)/sqrt(g0)))),'r','LineStyle',':')
%     end
%     axis([min(t(xplot)/sqrt(g0)) max(t(xplot)/sqrt(g0))  0 Tmax])
%     hold off
%     ax1 = gca; % current axes
%     ax1_pos = ax1.Position; % position of first axes
%     ax2 = axes('Position',ax1_pos,...
%         'XAxisLocation','top',...
%         'YAxisLocation','right',...
%         'Color','none');
%     line(H.*K,X,'Parent',ax2,'Color',[0.87,0.74,0.53],'LineWidth',1.2)
%     ax2.YLim=([0. Tmax]);
%     set(gca,'xlim',[0 5*max(H.*K)],'ytick',[] ,'xtick',[ 0 min(H.*K)  max(H.*K)*1.0000001 ],'xticklabel',{'hk:' min(H.*K)  max(H.*K)})
% 
%     
%     f=figure('position',[20 20 500 500]);
%     hold on
%     UdBth = -60;
%     Usp = 20*log10(abs(uspectrum.')*sqrt(g0)*(1/(2*pi))/U0);
%     Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
%     imagesc(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,Usp);
%     set(gca,'YDir','normal','FontSize',14);
%     xlim([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)]);
%     ylim([min(X),max(X)]);
%     xlabel('f [Hz]','fontsize',16);
%     ylabel('X [m]','fontsize',16);
%     colorbar;
% %     plot(omegaxis*sqrt(g0)/(2*pi),-X0.*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'--g')
%     plot(omegaxis*sqrt(g0)/(2*pi),xvarstart.*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'color',[0.87,0.74,0.53],'LineStyle','--')
%     plot(omegaxis*sqrt(g0)/(2*pi),xvarstop.*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'color',[0.87,0.74,0.53],'LineStyle','--')
%     if K(ix)*H(ix)<(crit+0.02) & K(ix)*H(ix)>(crit-0.02);
%         plot(omegaxis*sqrt(g0)/(2*pi),X(ix).*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'r','LineStyle',':')
%     end
%     axis([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi) 4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)  0 Tmax])
%     hold off
%     ax1 = gca; % current axes
%     ax1_pos = ax1.Position; % position of first axes
%     ax2 = axes('Position',ax1_pos,...
%         'XAxisLocation','top',...
%         'YAxisLocation','right',...
%         'Color','none');
%     line(H.*K,X,'Parent',ax2,'Color',[0.87,0.74,0.53],'LineWidth',1.2)
%     ax2.YLim=([0. Tmax]);
%     set(gca,'xlim',[0 5*max(H.*K)],'ytick',[] ,'xtick',[ 0 min(H.*K)  max(H.*K)*1.0000001 ],'xticklabel',{'hk:' min(H.*K)  max(H.*K)})
% 
%     