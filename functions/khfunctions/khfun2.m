function kh = khfun2(x,kh0,khm,xvarstart,xvarstop,xslope)
    %%
    % From hk0 to hkm and back
    % INPUT
    % x point in the propagation distance
    % kh0 initial kh
    % khm maximum kh
    % xvarstart/stop initial/final coordinate of kh variation
    % xslope, a normalization for the transition
    % 
    % OUTPUT
    % kh(x)
    %%
    xvarm = xvarstart + 0.5*(xvarstop-xvarstart);
    kh = zeros(size(x));
    kh(x<=xvarstart) = kh0;
    kh(x>xvarstop) = kh0;
    kh(x>xvarstart & x<=xvarm) = khm - 0.5*(khm-kh0).*erfc((x(x>xvarstart & x<=xvarm) - 0.5*(xvarstart+xvarm))/xslope);
    kh(x>xvarm & x<=xvarstop) = kh0 + 0.5*(khm-kh0).*erfc((x(x>xvarm & x<=xvarstop) - 0.5*(xvarstop+xvarm))/xslope);
    
end