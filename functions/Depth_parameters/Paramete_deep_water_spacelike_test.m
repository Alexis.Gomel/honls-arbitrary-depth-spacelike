%%% Sanity chek for the parameter convergence between Dysthe and finite depth.
clear all 
clc
close all
crit= 1.363;

kh=[0.6:0.001:20];
omega=1;
oness=ones(length(kh),1);
sigma = tanh(kh);
k = omega.^2./sigma;
h = kh.*sigma./omega^2;
% group and phase speed
cp = omega./k;
cg = (sigma + kh.*(1-sigma.^2))/2./omega;
x=[1];
FOD=1;
ON_OFF_coef=[1,1,1,1,1,1];
[h,k,sigma,cg,cp,lambda,nu,alpha,alpha4,beta,gamma,mu,muintexp,Dys_hilb_gen] = depthdependent_HONLSparameters_onoff(omega,kh,FOD,ON_OFF_coef);
[h2,k2,sigma2,cg2,cp2,lambda2,nu2,alpha2,alpha42,beta2,gamma2,mu2,Dys_hilb,muintexp] = Dysthe_space_HONLSparameters(omega,kh,x,FOD);

nu_g=((sigma+1).^2.*kh-sigma).*((sigma-1).^2.*kh-sigma);
mu_g=2.*sigma./omega.*(2.*omega-k.*cg.*(sigma.^2-1));
D=-h.*omega.*k.*mu_g./(2.*sigma.*nu_g);
	
b_D_space=omega.*k./(16*sigma.^4).*(2.*sigma.^6 - 13*sigma.^4 + 12*sigma.^2-9);
b_D=1./(16*sigma.^4).*(2.*sigma.^6 - 13*sigma.^4 + 12*sigma.^2-9);
b_corr=-mu_g.^2./(8.*sigma.^2.*nu_g);
q3=b_D+b_corr;
nu_D = -b_D.*k.^2.*omega./cg;

b_D_space_deep=-omega.* ones(1,length(k)).*k(end).^2/2;
figure()
hold on
plot(kh,b_D_space,'-k','LineWidth',1.5)
plot(kh,b_D_space_deep,'--k','LineWidth',1.5)
% plot([crit, crit], [min(cg), max(cg)],':k','LineWidth',1.2)
ylabel('$\hat{B}_D$','interpreter','latex','FontSize',15)
xticks(sort([crit,linspace(0,max(kh),6)]))
hold off

figure()
hold on
plot(kh,cg,'-k','LineWidth',1.5)
plot(kh,cg2.*oness,'--k')
plot([crit, crit], [min(cg), max(cg)],':k','LineWidth',1.2)
ylabel('$C_g$','interpreter','latex','FontSize',15)
xticks(sort([crit,linspace(0,max(kh),6)]))

hold off
xlabel('$kh$','interpreter','latex','FontSize',15)
%%
hat_alpha= -cg.^3.*(1./(2.*omega.*cg)).*(1-h./(cg.^2).*(1-kh.*sigma).*(1-sigma.^2)); %space-like dispersion.. this is OK.
hat_alpha_deep=-omega./(8.*k.^2);
bhat_D=-(omega.*k.^2)./(16.*sigma.^4).*(2.*sigma.^6 - 13.*sigma.^4 + 12.*sigma.^2-9);%space-like nonlinearity.. this is OK.
bhat_D_deep=(omega.*k.^2)./2;

b_corr=-mu_g.^2./(8.*sigma.^2.*nu_g);
b_hat_meanflow=-b_corr.*k.^2.*omega;%mean flow correction for shallow water under approximation.
b_hat=bhat_D+b_hat_meanflow;
figure()

subplot(211)
hold on
yyaxis left
plot(kh,hat_alpha,'-b','DisplayName','$$\hat{\alpha}$$','LineWidth',1.5)
plot(kh,hat_alpha_deep(end)*oness,'--b','DisplayName','Deep water')
yyaxis right
plot(kh,bhat_D,'-r','DisplayName','$$\hat{\beta}_D$$','LineWidth',1.5) 
plot(kh,bhat_D_deep(end).*oness,'--r','DisplayName','Deep water')
ylabel('Non-Linearity','interpreter','latex','FontSize',15)
legend({'$$\hat{\alpha}$$','Deep water','$$\hat{\beta}_D$$','Deep water'},'Interpreter','latex')
set(gca,'YColor','r')
ylim([0,5])
hold off
xlabel('kh')
legend()
xlim([0,10])
title('NLS space-like coeficcients')
subplot(223)
hold on
plot(kh,bhat_D,'--r','DisplayName','$$\hat{\beta_D}$$','LineWidth',1.5) 
plot(kh,b_hat_meanflow,':r','DisplayName','$$\hat{\beta_{correction}}$$','LineWidth',1.5) 
plot(kh,b_hat,'r','DisplayName','$$\hat{\beta}$$','LineWidth',1.5) 
plot(kh,0.*oness,'--k')
xlabel('kh')
legend({'$$\hat{\beta}_D$$','$$\hat{\beta}_{correction}$$','$$\hat{\beta}$$'},'Interpreter','latex')
ylim([-2,2])
hold off
xlim([0,10])
legend()
title('nonlinear coefficient')
subplot(224)
hold on
plot(kh,abs(b_hat_meanflow)./abs(bhat_D),'-r','LineWidth',1.5) 
legend({'$$\frac{\hat{\beta}_{correction}}{\hat{\beta}_D}$$'},'Interpreter','latex')
xlabel('kh')
hold off
title('nonlinear coefficient ratio')

%%
figure()
hold on
yyaxis left
plot(kh,lambda,'-b','DisplayName','Finite depth','LineWidth',1.5)
plot(kh,lambda2.*oness,'--b','DisplayName','Deep water')
plot([crit, crit], [min(lambda), max(lambda)],':k','LineWidth',1.2,'DisplayName','Deep water')
ylabel('Dispersion','interpreter','latex','FontSize',15)
set(gca,'YColor','blue')
yyaxis right
nu_old = k.^2.*omega./(16*sigma.^4.*cg).*(9 - 10*sigma.^2 + 9*sigma.^4-...
         2.*sigma.^2.*cg.^2./(h-cg.^2).*(4*cp.^2./cg.^2 + 4* cp./cg.*(1-sigma.^2) + h./cg.^2.*(1-sigma.^2).^2));
plot(kh,nu_D,'--r','DisplayName','$$\beta_D$$','LineWidth',1.5) 
plot(kh,nu_old,':r','DisplayName','$$\beta old$$','LineWidth',1.5) 
plot(kh,nu2.*oness,'--r','DisplayName','Deep water')
plot(kh,nu,'-r','DisplayName','$$\beta_D + corrections$$','LineWidth',1.5)
ylabel('Non-Linearity','interpreter','latex','FontSize',15)
set(gca,'YColor','r')
xticks(sort([crit,linspace(0,max(kh),6)]))
hold off
ylim([-2,2])
legend()
xlabel('$kh$','interpreter','latex','FontSize',15)
title('NLS time-like coeficcients')

%%
ix_beta= detectzerocross(beta);
ix_gamma= detectzerocross(gamma);

figure()
hold on
yyaxis left
plot([min(kh), max(kh)],[0,0] ,'-k','LineWidth',0.8)
plot(kh,beta,'-b','DisplayName','|A|^2 D_t A','LineWidth',1.5)
plot(kh,beta2.*oness,'--b','DisplayName','|A|^2 D_t A (DW)')
plot([crit, crit], [min(beta), max(beta)],':k','LineWidth',1.2)
ylabel('$\mathcal{B}_{21}$','interpreter','latex','FontSize',15)
set(gca,'YColor','blue')
ylim([-15,10])
yyaxis right
plot(kh,gamma2.*oness,'--r','DisplayName','A D_t |A|^2 (DW)')
plot(kh,gamma,'-r','DisplayName','A D_t |A|^2','LineWidth',1.5)
set(gca,'YColor','red')
ylabel('$\mathcal{B}_{22}$','interpreter','latex','FontSize',15)
hold off
ylim([-15,20])
% legend()
xlabel('$kh$','interpreter','latex','FontSize',15)
xticks(sort([crit,linspace(0,max(kh),6)]))

axes('Position',[.45 .18 .4 .4])
box on
hold on
yyaxis left
plot([min(kh), max(kh)],[0,0] ,'-k','LineWidth',0.8)

plot(kh,beta,'-b','DisplayName','\beta_{21}','LineWidth',1.5)
plot(kh,beta2.*oness,'--b','DisplayName','\beta_{21} (DW)')
plot([crit, crit], [min(beta), max(beta)],':k','LineWidth',1.2)

% ylabel('$|A|^2 D_t A$','interpreter','latex')
set(gca,'YColor','blue')
ylim([-15,10])
xlim([0.7,1.6])
yyaxis right
plot(kh,gamma2.*oness,'--r','DisplayName','$\beta_{22}$ (DW)')
plot(kh,gamma,'-r','DisplayName','$\beta_{22}$','LineWidth',1.5)
set(gca,'YColor','red')
% ylabel('$A D_t |A|^2$','interpreter','latex')
hold off
ylim([-15,10])
xlim([min([kh(ix_beta),crit,kh(ix_gamma)])-0.15,1.8])
try
xticks(sort([kh(ix_beta),crit,kh(ix_gamma)]));
end
hold off
%%
% sigma2 = (2.*omega.*cg).^2 - 4 .* kh .* sigma;
% nu_paper_sigma=((sigma+1).^2.*kh-sigma).*((sigma-1).^2.*kh-sigma);
% nu_dw=1-4.*kh;
% figure()
% hold on
% plot(kh,sigma2)
% plot(kh,nu_paper_sigma)	
% plot(kh,nu_dw)
% xlabel('$kh$','interpreter','latex','FontSize',15)

%%
val_cross= detectzerocross(alpha);
figure()
hold on
% yyaxis left
plot(kh,alpha,'-b','LineWidth',1.5)
plot(kh,0.*oness,'--b')
plot([crit, crit], [min(alpha), max(alpha)],':k','LineWidth',1.2)
ylabel('Third order dispersion $\alpha_3$','interpreter','latex','FontSize',15)
set(gca,'YColor','blue')
% ylim([-15,10])
xticks(sort([kh(val_cross),crit,linspace(0,max(kh),6)]))
hold off
xlabel('$kh$','interpreter','latex','FontSize',15)

%%
% val_cross= detectzerocross(alpha);
figure()
hold on
% yyaxis left
plot(kh,D,'-b','LineWidth',1.5,'DisplayName','D ')
plot(kh,omega.*oness./2,'--b','DisplayName','\omega/2')
% plot([crit, crit], [min(alpha), max(alpha)],':k','LineWidth',1.2)
ylabel('D Sanity check','interpreter','latex','FontSize',15)
set(gca,'YColor','blue')
plot([crit, crit], [min(D), max(D)],':k','LineWidth',1.2)

legend()
ylim([0,1])
% xticks(sort([kh(val_cross),crit,linspace(0,max(kh),6)]))
hold off
xlabel('$kh$','interpreter','latex','FontSize',15)



%% D hilbert term

D_hilb_num=2+(1-sigma.^2).*cg./cp;
D_hilb_den=kh-sigma.*cg.^2./(cp.^2);
D_hilb_general=kh.*(D_hilb_num./D_hilb_den).*omega./(4.*sigma);
% D_hilb_2=(mu_g.*k./(4.*sigma.*cg.^2)).*D_hilb_2;
% val_cross= detectzerocross(alpha);
figure()
hold on
% yyaxis left
% plot(kh,D_hilb_2,'--b','LineWidth',1.5,'DisplayName','D\mu_g k/(4\sigma.c_g^2)_2 ')
plot(kh,Dys_hilb_gen,'-b','LineWidth',1.5,'DisplayName','D\mu_g k/(4\sigma.c_g^2) ')
plot(kh,2.*k.^3./omega,'--k','LineWidth',1.5,'DisplayName','2k^3/\omega')
% plot(kh,log(Dys_hilb_gen),'-b','LineWidth',1.5,'DisplayName','D\mu_g k/(4\sigma.c_g^2) ')
% plot(kh,log(2.*k.^3./omega),'--k','LineWidth',1.5,'DisplayName','2k^3/\omega')
plot([crit, crit], [min(Dys_hilb_gen), max(Dys_hilb_gen)],':k','LineWidth',1.2)
ylabel('$D\frac{\mu_g k}{4\sigma c_g^2}$','interpreter','latex','FontSize',15)
legend()
ylim([1.95,2.3])
% xlim([0.5,5])
hold off
xlabel('$kh$','interpreter','latex','FontSize',15)
% set(gca, 'YScale', 'log')
%% D hilbert term


figure()
hold on
plot(kh,log(Dys_hilb_gen),'-b','LineWidth',1.5,'DisplayName','ln(D\mu_g k/(4\sigma.c_g^2)) ')
plot(kh,log(2.*k.^3./omega),'--k','LineWidth',1.5,'DisplayName','ln(2k^3/\omega)')
plot([crit, crit], [min(log(Dys_hilb_gen)), max(log(Dys_hilb_gen))],':k','LineWidth',1.2)
ylabel('$ln(D\frac{\mu_g k}{4\sigma c_g^2})$','interpreter','latex','FontSize',15)
legend()
% ylim([1.95,40])
xlim([0.5,25])
hold off
xlabel('$kh$','interpreter','latex','FontSize',15)
set(gca, 'YScale', 'log')

%% Easy approximation
figure()
x=-10:0.01:10;
hold on
plot(x,1./(tanh(x)),'-r','LineWidth',1.5,'DisplayName','coth(kh)')
plot(x,1./x+sign(x),'--b','LineWidth',1.5,'DisplayName','1/kh+sgn(k)')
% plot(x,1./x+sign(x)-sign(x).*abs(x).^(-4/5),':b','LineWidth',1.5,'DisplayName','1/kh+sgn(k)')
xlabel('$kh$','interpreter','latex','FontSize',15)
ylim([-5,5])
legend()
hold off
%% easyer aprox
figure()
x=-10:0.01:10;
fnew=zeros(1,length(x));
for j=1:length(x)
	bla=1-abs(x(j))./2;
	if bla>0
	fnew(j)=bla;
	end
end
hold on
plot(x,1./(tanh(x)),'-r','LineWidth',1.5,'DisplayName','coth(kh)')
plot(x,1./x+sign(x),'--b','LineWidth',1.5,'DisplayName','1/kh+sgn(k)')
plot(x,1./(x.*(x.^2+1))+sign(x),'-k','LineWidth',1.5,'DisplayName','1/(x^2+1)')

plot(x,exp(-abs(x))./x+sign(x),'--k','LineWidth',1.5,'DisplayName','exp(-abs(kh))/kh+sgn(k)')
plot(x,(fnew)./x+sign(x),':k','LineWidth',1.5,'DisplayName','fnew+sgn(k)')
% plot(x,1./x+sign(x)-sign(x).*abs(x).^(-4/5),':b','LineWidth',1.5,'DisplayName','1/kh+sgn(k)')
xlabel('$kh$','interpreter','latex','FontSize',15)
ylim([-5,5])
legend()
hold off

%%
figure()
hold on
% plot(x,1./(tanh(x)),'-r','LineWidth',1.5,'DisplayName','coth(kh)')
plot(x,abs((1./x+sign(x)).*(tanh(x))),'--b','LineWidth',1.5,'DisplayName','1/kh+sgn(k)')
plot(x,abs((1./(x.*(x.^2+1))+sign(x)).*(tanh(x))),'-k','LineWidth',1.5,'DisplayName','1/(x^2+1)')

plot(x,abs((exp(-abs(x))./x+sign(x)).*(tanh(x))),'--k','LineWidth',1.5,'DisplayName','exp(-abs(kh))/kh+sgn(k)')
plot(x,abs(((fnew)./x+sign(x)).*(tanh(x) )),':k','LineWidth',1.5,'DisplayName','fnew+sgn(k)')
% plot(x,1./x+sign(x)-sign(x).*abs(x).^(-4/5),':b','LineWidth',1.5,'DisplayName','1/kh+sgn(k)')
xlabel('$kh$','interpreter','latex','FontSize',15)
ylim([1,1.6])
legend()
hold off
%% easyer aprox
figure()
x=-10:0.01:10;
fnew=zeros(1,length(x));
for j=1:length(x)
	bla=1-abs(x(j))./2;
	if bla>0
	fnew(j)=bla;
	end
end
hold on

plot(x,exp(-abs(x)),'--k','LineWidth',1.5,'DisplayName','exp(-abs(kh))')
plot(x,1./(x.^2+1),'-k','LineWidth',1.5,'DisplayName','1/(x^2+1)')

plot(x,(fnew),':k','LineWidth',1.5,'DisplayName','f piecewise')
% plot(x,1./x+sign(x)-sign(x).*abs(x).^(-4/5),':b','LineWidth',1.5,'DisplayName','1/kh+sgn(k)')
xlabel('$kh$','interpreter','latex','FontSize',15)
% ylim([-5,5])
legend()
hold off
%%
val_cross= detectzerocross(alpha4);
figure()
hold on
plot(kh,alpha4,'-k','LineWidth',1.5)
plot(kh,0.*oness,'--k')
plot([crit, crit], [min(alpha4), max(alpha4)],':k','LineWidth',1.2)
xticks(sort([kh(val_cross),linspace(0,max(kh),6)]))
ylabel('$\alpha_4$','interpreter','latex','FontSize',15)
xlabel('$kh$','interpreter','latex','FontSize',13)

%%
function zerocross = detectzerocross(x)
	test_cross=x(1:end-1).*x(2:end);
	zerocross= find(test_cross<0);
end