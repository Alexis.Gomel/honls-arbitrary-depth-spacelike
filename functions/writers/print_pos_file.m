function print_pos_file(filename,Input_function,input_freq,initial_depth,wm_factor,M)
	fileID = fopen(filename,'w');

	format shortg;
	datetime = clock;
	fprintf(fileID,'#EDL\tDate\t%4d.%2d.%2d\tLocal Time\t%2d:%2d:%.3f\tpos\n',datetime);
	fprintf(fileID,'Rate(Hz)\tinput_function\tinitial_depth_in_m\twm_factor\n');

	fprintf(fileID,'%.1f\t%',input_freq);
	fprintf(fileID,Input_function);
	fprintf(fileID,'\t%.2f\t%',initial_depth);
	fprintf(fileID,'%.2f\n%',wm_factor);

	fprintf(fileID,'gauges_position\tdepth\n');
	fprintf(fileID,'m\tm\n');	
	fprintf(fileID,'%.5f\t\n',M);
	fclose(fileID);
end

